<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property string $name
 * @property string $sub_name
 * @property string $properties
 * @property string $nutritional_value
 * @property string $description
 * @property string $orange_1
 * @property string $black_1
 * @property string $orange_2
 * @property string $black_2
 * @property string $orange_3
 * @property string $black_3
 * @property string $preview
 *
 * The followings are the available model relations:
 * @property ProductCategory[] $productCategories
 * @property ProductOrder[] $productOrders
 * @property ProductPromo[] $productPromos
 * @property ProductTasteVolume[] $productTasteVolumes
 */
class Product extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public $image;
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name, sub_name', 'length', 'max'=>255),
			array('orange_1, black_1, orange_2, black_2, orange_3, black_3', 'length', 'max'=>32),
			array('preview, image', 'length', 'max'=>64),
            array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, sub_name, properties, nutritional_value, description, orange_1, black_1, orange_2, black_2, orange_3, black_3, preview', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productCategories' => array(self::HAS_MANY, 'ProductCategory', 'id_product'),
			'productOrders' => array(self::HAS_MANY, 'ProductOrder', 'id_product'),
			'productTasteVolumes' => array(self::HAS_MANY, 'ProductTasteVolume', 'id_product'),
            'product_promo' => array(self::HAS_MANY, 'ProductPromo', 'id_product'),
            'promos'=> array(self::MANY_MANY,'Promo','product_promo(id_product,id_promo)'),
            'tastes'=>array(self::MANY_MANY,'Taste','product_taste_volume(id_product,id_taste)'),
            //'position'=>array(self::HAS_MANY,'ProductCategory','position')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'sub_name' => 'Девиз',
			'properties' => 'Особенности',
			'nutritional_value' => 'Пищевая Ценность',
			'description' => 'Описание',
			'orange_1' => 'Значение св-ва 1',
			'black_1' => 'Свойство 1',
			'orange_2' => 'Значение св-ва 2',
			'black_2' => 'Свойство 2',
			'orange_3' => 'Значение св-ва 3',
			'black_3' => 'Свойство 3',
			'preview' => 'Изображение',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('sub_name',$this->sub_name,true);
		$criteria->compare('properties',$this->properties,true);
		$criteria->compare('nutritional_value',$this->nutritional_value,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('orange_1',$this->orange_1,true);
		$criteria->compare('black_1',$this->black_1,true);
		$criteria->compare('orange_2',$this->orange_2,true);
		$criteria->compare('black_2',$this->black_2,true);
		$criteria->compare('orange_3',$this->orange_3,true);
		$criteria->compare('black_3',$this->black_3,true);
		$criteria->compare('preview',$this->preview,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function get_properties()
    {
        return implode('<br/>',unserialize($this->properties));
    }
    public function get__properties()
    {
        return unserialize($this->properties);
    }

    public function getCurrentImage(){
        $model = $this->product_promo;

        //print_r($allImage);
        $result = array();
        $i = 0;
        foreach( $model as $val ){
           $result[$i] = $val->idPromo->preview;
            $i++;
        }
        return $result;
    }

    public function getCurrentImageDoneView(){
        $model = $this->product_promo;
        $result = '';
        foreach( $model as $val ){
            $result .= '<img src="/uploads/promo/preview/'.$val->idPromo->preview.'"/>';
        }
        return $result;
    }

    public function getProductCategory(){
        $result = '';
        $model = $this->productCategories;
        foreach( $model as $val ){
            $result .= $val->nameCategory->name.'. Позиция: '.$val->position.'</br>';
        }
        return $result;
    }

    public function getPromoImage(){
        $result = '';
        $model = $this->product_promo;
        foreach( $model as $val ){
            $result .= $val->nameCategory->preview.';';
        }
        return $result;
    }
    public function getAllTasteVolume(){
        $result = '';
        $model = $this->productTasteVolumes;
        foreach( $model as $val ){
            $result .= 'Вкус: '.$val->idTaste->name.' '.'Объем: '.$val->idVolume->value.'<br/>';
        }
        return $result;
    }

    public function getAllPosition(){

        $result = '';
        $model = $this->productCategories;

        foreach( $model as $val ){
            $result .= 'Позици';
        }

    }

}
