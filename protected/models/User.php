<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property integer $data_create
 */
class User extends CActiveRecord
{
        private $_identity;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			/*array('password, data_create, login', 'required'),
			array('data_create', 'numerical', 'integerOnly'=>true),
			array('login', 'length', 'max'=>16),
			array('password', 'length', 'max'=>64),*/
                    
                    // login
                        array('login, password', 'required', 'on'=>'login'),
                        array('password', 'authenticate', 'on'=>'login'),
                    
                    // signup
                        
                    
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, login, password, date_create', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Имя пользователя',
			'password' => 'Пароль',
			'date_create' => 'Дата создания',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('date_create',$this->data_create);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        public function authenticate($attribute, $params)
        {
            if( !$this->hasErrors() ) {
                $this->_identity = new UserIdentity($this->login, $this->password);
                if( !$this->_identity->authenticate() ) {
                    $this->addError('password', 'Неверный логин или пароль.');
                }
            }
        }


        public function login()
        {
            if ( $this->_identity===null ) {
                $this->_identity=new UserIdentity($this->login, $this->password);
                $this->_identity->authenticate();
            }
            if ( $this->_identity->errorCode===UserIdentity::ERROR_NONE ) {
                $duration = 3600*24*30;
                Yii::app()->user->login($this->_identity, $duration);
                /*User::model()->updateByPk(Yii::app()->user->id, array(
                    'date_last_login'=>time(),
                ));*/
                return true;
            }
            else {
                return false;
            }
        }
        public function getUser(){
            $result = array();
            $model = $this->findAll();
            foreach( $model as $val ){
                $result[$val->id] = $val->name;
            }
            return $result;
        }
}
