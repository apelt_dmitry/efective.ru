<?php

class ContactForm extends CFormModel
{
    public $name;
    public $phone;
 
    public function rules()
    {
        return array(
            array('name, phone','required'),
            array('name', 'length', 'max'=>64),
            array('phone','length', 'max'=>32),
            //array('phone', 'match'/*'pattern'=>'/\d \(\)+\-/ui'*/),
        );
    }


    public function attributeLabels()
    {
        return array(
            'name'=>'Контактное лицо',
            'phone'=>'Телефон',
        );
    }
    
    public function goContact()
    {   

        include_once "libmail.php";
        $m = new Mail;
        //$m->From($this->email.';admin@google.com');
        $m->smtp_on('ssl://smtp.gmail.com', 'i525643@gmail.com', '3W84R3GWxty', '465');
        $m->From('allspectehnika24.ru'.';i525643@gmail.com');
        $m->To(array(Yii::app()->controller->config->adminEmail));
        $m->Subject('Тема письма');
        $m->Body('Отправитель: '.$this->name.'. <br/> Телефон: '.$this->phone);
        $m->Send();
    }
}
