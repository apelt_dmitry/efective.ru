<?php

/**
 * This is the model class for table "product_taste_volume".
 *
 * The followings are the available columns in table 'product_taste_volume':
 * @property integer $id_product
 * @property integer $id_taste
 * @property integer $id_volume
 *
 * The followings are the available model relations:
 * @property Volume $idVolume
 * @property Taste $idTaste
 * @property Product $idProduct
 */
class ProductTasteVolume extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_taste_volume';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_product, id_taste, id_volume', 'required'),
			array('id_product, id_taste, id_volume', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_product, id_taste, id_volume', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idVolume' => array(self::BELONGS_TO, 'Volume', 'id_volume'),
			'idTaste' => array(self::BELONGS_TO, 'Taste', 'id_taste'),
			'idProduct' => array(self::BELONGS_TO, 'Product', 'id_product'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_product' => 'Id Product',
			'id_taste' => 'Id Taste',
			'id_volume' => 'Id Volume',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_product',$this->id_product);
		$criteria->compare('id_taste',$this->id_taste);
		$criteria->compare('id_volume',$this->id_volume);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductTasteVolume the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
