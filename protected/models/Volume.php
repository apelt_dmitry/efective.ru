<?php

/**
 * This is the model class for table "volume".
 *
 * The followings are the available columns in table 'volume':
 * @property integer $id
 * @property string $value
 *
 * The followings are the available model relations:
 * @property ProductTasteVolume[] $productTasteVolumes
 */
class Volume extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'volume';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('value', 'required'),
			array('value', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productTasteVolumes' => array(self::HAS_MANY, 'ProductTasteVolume', 'id_volume'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Volume the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function items($id_product,$id_taste)
    {
        $result = array();
        $criteria = new CDbCriteria;
        $criteria->compare('id_product',$id_product);
        $criteria->compare('id_taste',$id_taste);
        //$criteria->order = '`name` ASC';
        $values = ProductTasteVolume::model()->findAll($criteria);

        foreach ( $values as $value ) {
            $result[] = array(
                'id'=>$value->id_volume,
                'value'=>Volume::model()->findByPk($value->id_volume)->value,
            );
        }

        return $result;
    }

}
