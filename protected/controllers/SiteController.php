<?php

class SiteController extends Controller
{
	public function actionIndex()
	{
            $this->redirect('/home');
	}
        
        public function actionMain()
        {
            $meta = MetaTags::model()->findByPk(1);
            $model = Product::model()->findByPk(3);
            $conf = Config::model()->findByPk(1);


            $this->render('index', array(
               'meta' => $meta,

            ));
        }
        
        public function actionContacts()
        {
            $meta = MetaTags::model()->findByPk(5);
            
            $this->render('contacts', array(
               'meta' => $meta,
            ));
        }

        public function actionCatalog()
        {
            $meta = MetaTags::model()->findByPk(4);

            $this->render('catalog', array(
                'meta' => $meta,
            ));
        }
        
        public function actionProducts($id=null)
        {
            $meta = MetaTags::model()->findByPk(3);
            $categories = Category::model()->findAll();

            if($id===null){
                $this->render('products', array(
                    'meta' => $meta,
                    'categories' => $categories,
                ));
            } else{
                $model = $this->loadProduct($id);
                $this->render('product', array(
                    'model' => $model,
                ));
            }

        }

        public function actionAbout()
        {
            $meta = MetaTags::model()->findByPk(2);

            $this->render('about', array(
                'meta' => $meta,
            ));
        }

        public function actionToorder()
        {
            $meta = MetaTags::model()->findByPk(4);
            //$products = Product::model()->findAll();
            $criteria = new CDbCriteria();
            $criteria->with = array('idProduct');
            $criteria->order = 'idProduct.name ASC';
            $product_taste_volume = ProductTasteVolume::model()->findAll($criteria);
            $this->render('to_order', array(
                'meta' => $meta,
                //'products' => $products,
                'product_taste_volume' => $product_taste_volume,
                //'model' => $model,
            ));
        }

        public function actionMapProduct()
        {
            $meta = MetaTags::model()->findByPk(6);

            $this->render('map_product', array(
                'meta' => $meta,
            ));
        }

        public function actionTest()
        {
            $this->render('test');
        }
     
        public function actionError()
	{
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
	}
       

        

        public function actionOrder()
        {
            $model = new OrderForm();

            if ( isset($_POST['ajax']) && $_POST['ajax']==='order-form' ) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if ( isset($_POST['OrderForm']) && isset($_POST['id_product']) ) {
                $model->attributes = $_POST['OrderForm'];
                $order = '';
                foreach($_POST['id_product'] as $key=>$val){
                    $productTasteVolume = ProductTasteVolume::model()->findByAttributes(array('id_product' => $val, 'id_taste' => $_POST['id_taste'][$key], 'id_volume' => $_POST['id_volume'][$key]));
                    $order .= ' '.$productTasteVolume->idProduct->name." ".' '.$productTasteVolume->idTaste->name." ".'('.$productTasteVolume->idVolume->value.")".' - '.$_POST['count'][$key].'шт.'."<br/>";
                }
                //Yii::app()->end();
                $model->my_order = $order;
                $model->date = time();
                $model->ip = Yii::app()->request->userHostAddress;

                if( $model->validate() ){
                    $model->save(false);
                    $model->goOrder();
                    echo '1';
                }else{
                    echo '0';
                }

            } else{
                $model->attributes = $_POST['OrderForm'];
                if ( $model->validate() ) {
                    Yii::app()->user->setFlash('success', true);
                    $model->date = time();
                    $model->ip = Yii::app()->request->userHostAddress;
                    $model->save(false);
                    $model->goContact();
                    echo '1';
                } else {
                    echo '0';
                    print_r($model->errors);
                }

            }
            $this->redirect(array('site/index'));
        }


        public function loadProduct($id)
        {
            $model = Product::model()->findByPk($id);
            if ( !$model ) {
                throw new CHttpException(404, 'Товар не найден.');
            }
            return $model;
        }

        public function action_ajax()
        {
            $r = Yii::app()->request;
            $c = $this->config;

            switch ( $r->getParam('action') ) {
                case 'getVolumes':
                    echo json_encode(Volume::items($r->getParam('id_product'),$r->getParam('id_taste')));
                    break;

                default:
                    echo json_encode(null);
                    break;
            }
        }



}