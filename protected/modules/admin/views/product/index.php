<?php echo BsHtml::pageHeader('Список продуктов') ?>

<?= BsHtml::linkButton('Добавить продукт', array(
    'icon' => BsHtml::GLYPHICON_PLUS,
    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
    'url' => array('create'),
    //'target'=>'_blank',
    //'block' => true,
    //'style'=>'float: right;',
)); ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>null,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
        array(
            'name'=>'id',
        ),
        array(
            'name'=>'name',
        ),
        
        array(
            'header' => 'Категории',
            'type' => 'raw',
            'value' => '$data->productCategory'
        ),

        array(
            'name' => 'preview',
            'type'=>'raw',
            'value' => 'BsHtml::image("/uploads/product/preview/".$data->preview)',
        ),

        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'viewButtonUrl'=>'Yii::app()->urlManager->createUrl("/produkti", array("id"=>$data->id))',
            'viewButtonOptions'=>array(
                'target'=>'_blank',
            ),
        ),
    ),
)); ?>