<script>
    function remove_taste_volume(id_rem){
        $('#'+id_rem+'').remove();
    }
    $(document).ready(function(){
        $('#taste_id,#volume_id').attr('name','');
        $('#new_taste_volume').click(function(){
            if( $('#taste_id').val()!="" && $('#volume_id').val()!="" ){
                var MT =Math.ceil(Math.random() * (999999999999 - 1) + 1);
                console.log(MT);
                $('#data_taste_volume').append('<tr id="'+ MT +'"><td>'+ $('#taste_id option[value="'+ $('#taste_id').val() +'"]').text() +'<input type="hidden" name="taste_id[]" value="'+ $('#taste_id').val() +'"/></td><td>'+ $('#volume_id option[value="'+ $('#volume_id').val() +'"]').text() +'<input type="hidden" name="volume_id[]" value="'+ $('#volume_id').val() +'"/></td><td><button onclick="remove_taste_volume('+MT+');" class="btn btn-danger btn-xs" type="button">Удалить</button></td></tr>');
            }
        } )
    });
</script>
<script>
    function remove_taste_volume(id_remove){
        $('#'+id_remove+'').remove();
    }
    $(document).ready(function(){
        $('#category_id').attr('name','');
        $('#new_product_category').click(function(){
            if( $('#category_id').val()!=""){
                var MT_R =Math.ceil(Math.random() * (999999999999 - 1) + 1);
                console.log(MT_R);
                $('#data_product_category').append('<tr id="'+ MT_R +'"><td>'+ $('#category_id option[value="'+ $('#category_id').val() +'"]').text() +'<input type="hidden" name="category_id[]" value="'+ $('#category_id').val() +'"/></td><td><input name="position_product[]" value="0"></td><td><button onclick="remove_taste_volume('+MT_R+');" class="btn btn-danger btn-xs" type="button">Удалить</button></td></tr>');
            }
        } )
    });
</script>
<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'menu-form',
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>false,
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?= $form->fileFieldControlGroup($model,'image'); ?>

    <?php echo $form->textFieldControlGroup($model,'name'); ?>

    <?php echo $form->textFieldControlGroup($model,'sub_name'); ?>

    <label class="col-lg-2 text-right">
        Добавить категорию и позицию
    </label>
    <div class=" col-lg-4">
        <select class="form-control" name="" id="category_id">
            <option value="" selected="selected">Выберите...</option>
            <?php
            $allCategory = Category::model()->allCategory;
            foreach( $allCategory as $key => $val ): ?>
                <option value="<?=$key; ?>"><?=$val; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class=" col-lg-4">
    </div>
    <div class="col-lg-2">
        <?= BsHtml::button('Добавить',array('id'=>'new_product_category'))?>
    </div>
    <div class="row">
        <div class="col-lg-4">

        </div>
        <div class="col-lg-8">
            <table class="table" id="data_product_category" >

                <?php if( $scenario == 'update' ){

                    $productCategory = ProductCategory::model()->findAllByAttributes(array('id_product' => $model->id));
                    echo '<tbody>';
                    foreach( $productCategory as $val ):
                        ?>

                        <tr id="<?=$val->id; ?>">
                            <td><?=$val->nameCategory->name; ?><input type="hidden" name="category_id[]" value="<?=$val->nameCategory->id; ?>"></td>
                            <td><input name="position_product[]" value="<?=$val->position ?>"></td>
                            <td><button onclick="remove_taste_volume(<?=$val->id; ?>);" class="btn btn-danger btn-xs" type="button">Удалить</button></td>
                        </tr>

                    <?php endforeach; ?>

                    <?php echo '</tbody>'; } ?>

            </table>
        </div>
    </div>



<label class="col-lg-2 text-right">
    Добавить промо картинку
</label>
<div class="col-lg-10">


    <?php $allPromo = Promo::model()->allPromo; if( $scenario == 'update' ){ ?>

        <?php

        foreach( $allPromo as  $key => $val ): ?>

            <?php $productPromo = ProductPromo::model()->findByAttributes(array('id_promo' => $key)); if( $productPromo->id != null ){ ?>

                <label class="checkbox inline">
                    <input type="checkbox" name="check_image[]" checked value="<?=$key ?>"><img src="/uploads/promo/<?= $val ?>">
                </label>

            <?php } else{ ?>

                <label class="checkbox inline">
                    <input type="checkbox" name="check_image[]" value="<?=$key ?>"><img src="/uploads/promo/<?= $val ?>">
                </label>

            <?php } ?>
        <?php endforeach; ?>

    <?php }else{ ?>

        <?php

        foreach( $allPromo as  $key => $val ): ?>
            <label class="checkbox inline">
                <input type="checkbox" name="check_image[]" value="<?=$key ?>"><img src="/uploads/promo/<?= $val ?>">
            </label>
        <?php endforeach; ?>

    <?php } ?>

</div>






    <?php echo $form->textAreaControlGroup($model,'properties'); ?>

    <?php echo $form->textFieldControlGroup($model,'black_1'); ?>
    <?php echo $form->textFieldControlGroup($model,'orange_1'); ?>
    <?php echo $form->textFieldControlGroup($model,'black_2'); ?>
    <?php echo $form->textFieldControlGroup($model,'orange_2'); ?>
    <?php echo $form->textFieldControlGroup($model,'black_3'); ?>
    <?php echo $form->textFieldControlGroup($model,'orange_3'); ?>


    <div class="form-group">
        <?= $form->labelEx($model, 'description', array(
            'class'=>'control-label col-lg-2',
        )) ?>
        <div class="col-lg-10">
            <?php $this->widget('ext.ckeditor.CKEditorWidget', array(
                'model' => $model,
                'attribute' => 'description',
                'defaultValue' => $model->description,
                'config' => array(
                    //'height' => '400px',
                    //'width' => '100%',
                ),
            )); ?>
        </div>
    </div>

    <div class="form-group">
        <?= $form->labelEx($model, 'nutritional_value', array(
            'class'=>'control-label col-lg-2',
        )) ?>
        <div class="col-lg-10">
            <?php $this->widget('ext.ckeditor.CKEditorWidget', array(
                'model' => $model,
                'attribute' => 'nutritional_value',
                'defaultValue' => $model->nutritional_value,
                'config' => array(
                    //'height' => '400px',
                    //'width' => '100%',
                ),
            )); ?>
        </div>
    </div>

    <div class="row">
        <label class="col-lg-2 text-right">Добавление вкусо-объёма</label>
        <div class="col-lg-4">

            <?php
                $taste_list = array(array('id'=>0,'name'=>'Выберите...'));
                $mass = Taste::model()->findAll();
                $count=1;
                foreach( $mass as $val ){
                    $taste_list[$count]['id'] = $val->id;
                    $taste_list[$count]['name'] = mb_strtoupper($val->name, 'UTF-8');
                    $count++;
                }
            ?>
            <?= BsHtml::dropDownList('taste_id', null, BsHtml::listData($taste_list, 'id','name'));?>
        </div>
        <div class="col-lg-4">
            <?= BsHtml::dropDownList('volume_id',null,BsHtml::listData(array(array('id'=>0,'value'=>'Выберите...'))+Volume::model()->findAll(),'id','value'));?>
        </div>
        <div class="col-lg-2">
            <?= BsHtml::button('Добавить',array('id'=>'new_taste_volume'))?>
        </div>



    </div>
    <div class="row">
        <div class="col-lg-2">

        </div>
        <div class="col-lg-10">
            <table class="table" id="data_taste_volume" >

                <?php if( $scenario == 'update' ){

                    $productTasteVolume = ProductTasteVolume::model()->findAllByAttributes(array('id_product' => $model->id));
                    echo '<tbody>';
                    foreach( $productTasteVolume as $val ):
                    ?>

                        <tr id="<?=$val->id; ?>">
                            <td><?=$val->idTaste->name; ?> <input type="hidden" name="taste_id[]" value="<?=$val->idTaste->id; ?>"></td>
                            <td><?=$val->idVolume->value; ?><input type="hidden" name="volume_id[]" value="<?=$val->idVolume->id; ?>"></td>
                            <td><button id_con="<?=$val->id; ?>" onclick="remove_taste_volume(<?=$val->id; ?>);" class="btn btn-danger btn-xs" type="button">Удалить</button></td>
                        </tr>

                    <?php endforeach; ?>

                <?php echo '</tbody>'; } ?>
            </table>
        </div>
    </div>


    <?= BsHtml::formActions(array(
        BsHtml::linkButton('Отмена', array(
            'color' => BsHtml::BUTTON_COLOR_DANGER,
            'icon' => BsHtml::GLYPHICON_REFRESH,
            'url' => '/admin/product/index'
        )),
        BsHtml::submitButton('Применить', array(
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'icon' => BsHtml::GLYPHICON_FLOPPY_SAVE,
            'name'=>'ok2',
        )),
        BsHtml::submitButton('Сохранить', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon'=> BsHtml::GLYPHICON_FLOPPY_SAVED,
            'name'=>'ok',
        )),
    ), array('class'=>'form-actions')); ?>

<?php $this->endWidget(); ?>
