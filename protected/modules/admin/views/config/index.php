<?php
    $this->pageTitle = Yii::app()->name.' - '.'Конфигурация системы';
?>
<?php $this->beginWidget('bootstrap.widgets.BsPanel', array(
    'title' => 'Конфигурация системы',
)); ?>

    <?php if ( Yii::app()->user->getFlash('success') ): ?>
        <?= BsHtml::alert(BsHtml::ALERT_COLOR_INFO, 'Информация обновлена.') ?>
    <?php endif; ?>

    <?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id'=>'config-form',
        'enableAjaxValidation'=>false,
        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnChange'=>true,
            'validateOnSubmit'=>true,
        ),
    )); ?>

        <?= $form->errorSummary($model); ?>

        <div class="panel panel-success">
            <div class="panel-heading">Изменение телефона сайта </div>
            <div class="panel-body">
                <?= $form->textFieldControlGroup($model,'phone',array(
                    'maxlength'=>16,
                )); ?>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">Настройки почты для отправки писем(gmail.com)</div>
            <div class="panel-body">
                <?= $form->textFieldControlGroup($model,'mail_order',array(
                    'maxlength'=>128,
                )); ?>
                <?= $form->textFieldControlGroup($model,'password_mail_order',array(
                    'maxlength'=>128,
                )); ?>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">Почта получателя заказов</div>
            <div class="panel-body">
                <?= $form->textFieldControlGroup($model,'manager_order_mail',array(
                    'maxlength'=>128,
                )); ?>
            </div>
        </div>

        <div class="panel panel-warning">
            <div class="panel-heading">Почта получателя (формы обратной связи)</div>
            <div class="panel-body">
                <?= $form->textFieldControlGroup($model,'manager_mail_scroll',array(
                    'maxlength'=>128,
                )); ?>
            </div>
        </div>

        <div class="panel panel-danger">
            <div class="panel-heading">Скрипт Яндекс метрики</div>
            <div class="panel-body">
                <?= $form->textAreaControlGroup($model,'yandex', array('rows' => 10)); ?>
            </div>
        </div>
    <div class="panel panel-danger">
        <div class="panel-heading">Изменение пароля</div>
        <div class="panel-body">
            <div class="form-group">
                <label class="control-label col-lg-2" for="User_password">Старый пароль</label>
                <div class="col-lg-10"><input maxlength="16" name="oldPassword" class="form-control" placeholder="Старый пароль" type="text" value=""></div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2" for="User_password">Новый пароль</label>
                <div class="col-lg-10"><input maxlength="16" name="newPassword" class="form-control" placeholder="Новый пароль" type="text" value=""></div>
            </div>
        </div>
    </div>

        <?= BsHtml::formActions(array(
        BsHtml::submitButton('Применить', array(
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'icon' => BsHtml::GLYPHICON_FLOPPY_SAVE,
            'name'=>'ok2',
        )),
        ), array('class'=>'form-actions')); ?>

    <?php $this->endWidget(); ?>

<?php $this->endWidget(); ?>