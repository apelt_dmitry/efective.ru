<script>
    $('#export-button').on('click',function() {
        $.fn.yiiGridView.export();
    });
    $.fn.yiiGridView.export = function() {
        $.fn.yiiGridView.update('dates-grid',{
            success: function() {
                $('#dates-grid').removeClass('grid-view-loading');
                window.location = '". $this->createUrl('exportFile')  . "';
            },
            data: $('.search-form form').serialize() + '&export=true'
        });
    }
</script>
<?= BsHtml::linkButton('Экспорт записей',array(
    'url'=>array('/admin/orderForm/orders'),
    'style'=>'float: right;',
    'color' => BsHtml::BUTTON_COLOR_DANGER,
)); ?>
<?php echo BsHtml::pageHeader('Заявки') ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
        array(
            'name'=>'id',
        ),
        array(
            'name' => 'date',
            'value' => 'Yii::app()->dateFormatter->format(\'HH:mm d MMM yyyy г.\', $data->date)',
            'filter' => false,
        ),
        array(
            'name'=>'ip',
        ),
        array(
            'name'=>'name',
            'value'=>'$data->Data',
            'type'=>'raw',
        ),
        array(
            'name'=>'commit',
        ),
        array(
            'name'=>'my_order',
            'type'=>'raw',
        ),
        array(
            'name' => 'subscr',
            'value' => '$data->subscribeResult',
        ),
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'template' => '{delete}'
        ),
    ),
)); ?>


