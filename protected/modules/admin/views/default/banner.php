<?php if ( Yii::app()->user->getFlash('success') ): ?>
    <?= BsHtml::alert(Bshtml::ALERT_COLOR_INFO, 'Информация обновлена.') ?>
<?php endif; ?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => true,
    //'id' => 'user_form',
    'htmlOptions' => array(
        'class' => 'bs-example'
    )
));
?>
    <div class="form-group">
        <?= $form->labelEx($model, 'banner1', array(
            'class'=>'control-label col-lg-2',
        )) ?>
        <div class="col-lg-10">
            <?php $this->widget('ext.ckeditor.CKEditorWidget', array(
                'model' => $model,
                'attribute' => 'banner1',
                'defaultValue' => $model->banner1,
                'config' => array(
                    //'height' => '400px',
                    //'width' => '100%',
                ),
            )); ?>
        </div>
    </div>

    <div class="form-group">
        <?= $form->labelEx($model, 'banner2', array(
            'class'=>'control-label col-lg-2',
        )) ?>
        <div class="col-lg-10">
            <?php $this->widget('ext.ckeditor.CKEditorWidget', array(
                'model' => $model,
                'attribute' => 'banner2',
                'defaultValue' => $model->banner2,
                'config' => array(
                    //'height' => '400px',
                    //'width' => '100%',
                ),
            )); ?>
        </div>
    </div>

    <div class="form-group">
        <?= $form->labelEx($model, 'banner3', array(
            'class'=>'control-label col-lg-2',
        )) ?>
        <div class="col-lg-10">
            <?php $this->widget('ext.ckeditor.CKEditorWidget', array(
                'model' => $model,
                'attribute' => 'banner3',
                'defaultValue' => $model->banner3,
                'config' => array(
                    //'height' => '400px',
                    //'width' => '100%',
                ),
            )); ?>
        </div>
    </div>

<?php
echo BsHtml::formActions(array(
    BsHtml::submitButton('Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_PRIMARY
    ))
));
?>
<?php
$this->endWidget();
?>