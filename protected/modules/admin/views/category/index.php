<?php echo BsHtml::pageHeader('Список категорий') ?>

<?= BsHtml::linkButton('Добавить категорию', array(
    'icon' => BsHtml::GLYPHICON_PLUS,
    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
    'url' => array('create'),
    //'target'=>'_blank',
    //'block' => true,
    'style'=>'float: right;',
)); ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>null,

    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,

    'columns'=>array(
        array(
            'name' => 'id'
        ),

        array(
            'name' => 'name'
        ),
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            //'template' => '{create} {update} {delete}',

        ),
    ),
)); ?>