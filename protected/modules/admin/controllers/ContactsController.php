<?php

class ContactsController extends AdminController
{
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if ( isset($_POST['Contacts']) ) {
            $model->attributes = $_POST['Contacts'];
            if( $model->save() ) {
                $this->redirect(array('index'));
            }   
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }

    public function actionIndex()
    {
        $model = new Contacts('search');
        $model->unsetAttributes();
        if ( isset($_GET['Contacts']) ) {
            $model->attributes=$_GET['Contacts'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=Contacts::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
