<?php

class ProductController extends AdminController
{


    public function actionCreate()
    {
        $model = new Product();
        if ( isset($_POST['Product']) ) {
            $model->attributes = $_POST['Product'];
            $model->image=CUploadedFile::getInstance($model,'image');
           // $model->properties = str_replace("\n",';',trim($model->properties));
            $properties = explode("\n",trim($model->properties));
            $result = array();
            foreach ($properties as $pro){
                $result[] = trim($pro);
            }
            $model->properties = serialize($result);



            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/product/'.$imageName);
                    $image->resize(150, 150);
                    $image->save('./uploads/product/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){
                    foreach($_POST['taste_id'] as $key=>$val){
                        $ptv = new ProductTasteVolume();
                        $ptv->id_product = $model->id;
                        $ptv->id_taste = $val;
                        $ptv->id_volume = $_POST['volume_id'][$key];
                        $ptv->save();
                    }
                    if( isset( $_POST['category_id'] ) ){
                        foreach( $_POST['category_id'] as $key => $val ){
                            $ProductCategory = new ProductCategory();
                            $ProductCategory->id_category = $val;
                            $ProductCategory->id_product = $model->id;
                            $ProductCategory->position = $_POST['position_product'][$key];
                            $ProductCategory->save(false);
                        }
                    }
                    if( isset( $_POST['check_image'] ) ){
                        foreach( $_POST['check_image'] as $val ){
                            $ProductPromo = new ProductPromo();
                            $ProductPromo->id_promo = $val;
                            $ProductPromo->id_product = $model->id;
                            $ProductPromo->save(false);
                        }
                    }
                    //$this->redirect(array('index'));

                }
            }
        }

        $this->render('create', array(
            'model'=>$model,
        ));

    }


    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if(isset($_POST['ok'])){
            if ( isset($_POST['Product']) ) {
                $model->attributes = $_POST['Product'];

                $properties = explode("\n",trim($model->properties));
                $result = array();
                foreach ($properties as $pro){
                    $result[] = trim($pro);
                }
                $model->properties = serialize($result);

                $model->image=CUploadedFile::getInstance($model,'image');
                // $model->properties = str_replace("\n",';',trim($model->properties));

                if($model->validate()){
                    $file = $model->image;
                    if ( $file->name!='' ) {
                        $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                        $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                        $image = Yii::app()->image->load($file->tempName);
                        $image->save('./uploads/product/'.$imageName);
                        $image->resize(150, 150);
                        $image->save('./uploads/product/preview/'.$imageName);
                        $model->preview = $imageName;
                    }
                    if($model->save(FALSE)){

                        ProductTasteVolume::model()->deleteAllByAttributes(array('id_product' => $model->id));
                        foreach($_POST['taste_id'] as $key=>$val){
                            $ptv = new ProductTasteVolume();
                            $ptv->id_product = $model->id;
                            $ptv->id_taste = $val;
                            $ptv->id_volume = $_POST['volume_id'][$key];
                            $ptv->save();
                        }

                        ProductCategory::model()->deleteAllByAttributes(array('id_product' => $model->id));
                        foreach( $_POST['category_id'] as $key => $val ){
                            $ProductCategory = new ProductCategory();
                            $ProductCategory->id_category = $val;
                            $ProductCategory->id_product = $model->id;
                            $ProductCategory->position = $_POST['position_product'][$key];
                            $ProductCategory->save(false);
                        }

                        ProductPromo::model()->deleteAllByAttributes(array('id_product' => $model->id));
                        foreach( $_POST['check_image'] as $val ){
                            $ProductPromo = new ProductPromo();
                            $ProductPromo->id_promo = $val;
                            $ProductPromo->id_product = $model->id;
                            $ProductPromo->save(false);
                        }

                    $this->redirect(array('index'));

                    }
                }
            }
        }
        elseif(isset($_POST['ok2'])){
            if ( isset($_POST['Product']) ) {
                $model->attributes = $_POST['Product'];

                $properties = explode("\n",trim($model->properties));
                $result = array();
                foreach ($properties as $pro){
                    $result[] = trim($pro);
                }
                $model->properties = serialize($result);

                $model->image=CUploadedFile::getInstance($model,'image');
                if($model->validate()){
                    $file = $model->image;
                    if ( $file->name!='' ) {
                        $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                        $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                        $image = Yii::app()->image->load($file->tempName);
                        $image->save('./uploads/product/'.$imageName);
                        $image->resize(150, 150);
                        $image->save('./uploads/product/preview/'.$imageName);
                        $model->preview = $imageName;
                    }
                    if($model->save(FALSE)){

                        ProductTasteVolume::model()->deleteAllByAttributes(array('id_product' => $model->id));
                        foreach($_POST['taste_id'] as $key=>$val){
                            $ptv = new ProductTasteVolume();
                            $ptv->id_product = $model->id;
                            $ptv->id_taste = $val;
                            $ptv->id_volume = $_POST['volume_id'][$key];
                            $ptv->save();
                        }

                        ProductCategory::model()->deleteAllByAttributes(array('id_product' => $model->id));
                        foreach( $_POST['category_id'] as $key => $val ){
                            $ProductCategory = new ProductCategory();
                            $ProductCategory->id_category = $val;
                            $ProductCategory->id_product = $model->id;
                            $ProductCategory->position = $_POST['position_product'][$key];
                            $ProductCategory->save(false);
                        }

                        ProductPromo::model()->deleteAllByAttributes(array('id_product' => $model->id));
                        foreach( $_POST['check_image'] as $val ){
                            $ProductPromo = new ProductPromo();
                            $ProductPromo->id_promo = $val;
                            $ProductPromo->id_product = $model->id;
                            $ProductPromo->save(false);
                        }
                    }
                }
            }
        }
        $model->properties = implode("\n",unserialize($model->properties));

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        ProductCategory::model()->deleteAllByAttributes(array('id_product'=>$id));
        ProductPromo::model()->deleteAllByAttributes(array('id_product'=>$id));
        ProductTasteVolume::model()->deleteAllByAttributes(array('id_product'=>$id));
        $model->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Product('search');
        $model->unsetAttributes();
        if ( isset($_GET['Product']) ) {
            $model->attributes=$_GET['Product'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Product::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='product-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


}
