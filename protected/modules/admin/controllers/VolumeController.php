<?php

class VolumeController extends AdminController
{


    public function actionCreate()
    {
        $model = new Volume();

        if ( isset($_POST['Volume']) ) {
            $model->attributes = $_POST['Volume'];
            if( $model->save() ) {
               // $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'model'=>$model,
        ));
    }


    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if ( isset($_POST['Volume']) && isset($_POST['ok2'])) {
            $model->attributes = $_POST['Volume'];
            if( $model->save() ) {
                $this->redirect(array('index'));
            }
        } elseif (isset($_POST['Volume']) && isset($_POST['ok'])) {
        $model->attributes = $_POST['Volume'];
            if( $model->save() ) {
              //  $this->redirect(array('index'));
            }
    }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {
        
        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Volume('search');
        $model->unsetAttributes();
        if ( isset($_GET['Volume']) ) {
            $model->attributes=$_GET['Volume'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Volume::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


}
