<?php

class OrderFormController extends AdminController
{
    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function actionIndex()
    {
        
        $model = new OrderForm('search');
        $model->unsetAttributes();
        if ( isset($_GET['OrderForm']) ) {
            $model->attributes=$_GET['OrderForm'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=OrderForm::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
    public function conv(){
        $con = array(array());
        $mod = OrderForm::model()->findAll();
        foreach( $mod as $key => $val ){
            $con[$key][id] = $val->id;
            $con[$key][name] = $val->name;
            $con[$key][phone] = $val->phone;
            $con[$key][email] = $val->email;
            $con[$key][face] = $val->face;
            $con[$key][commit] = $val->commit;
            $con[$key][date] = Yii::app()->dateFormatter->format('HH:mm d MMM yyyy г.', $val->date);
            $con[$key][subscr] = $val->subscribeResult;
            $con[$key][my_order] = $val->my_order;
            $con[$key][ip] = $val->ip;
        }
        return $con;
    }
    
    public function actionOrders() {
        include 'array-to-csv.php';
        $csv = new arrayToCsv();
        $arr = OrderForm::model()->findAll();
        
        $arr = $this->conv();
        
        $people = $csv->convert($arr);
        $file = '/tmp/people.csv';
        $people = iconv ('utf-8', 'windows-1251', $people);
		$people = str_replace("<br/>", "\r\n", $people);
        $first_text = "Номер записи ; Имя ; Телефон ; Email ; Юр.  лицо ; Комментарий ; Дата ; Подписка ; Заказ ; ip  \r\n";
        $first_text = iconv ('utf-8', 'windows-1251', $first_text);
        file_put_contents($file, $first_text);
        file_put_contents($file, $people,FILE_APPEND);
        $this->file_force_download($file);        
    }
    
    public function file_force_download($file) {
        if (file_exists($file)) {
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
                ob_end_clean();
            }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            readfile($file);
            unlink($file);
            exit;
        }
    }
}
