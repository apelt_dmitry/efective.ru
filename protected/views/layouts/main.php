<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    // bootstrap 3.3.1
    ->registerCssFile($pt.'css/bootstrap.min.css')
    // bootstrap theme
    ->registerCssFile($pt.'css/bootstrap-theme.min.css')
    //preLoader
    ->registerCssFile($pt.'css/preLoader.css')
    //less
    //->registerCssFile($pt.'css/first.less')
    //slick
    ->registerCssFile($pt.'css/slick.css')
    ->registerCssFile($pt.'css/slick-theme.css')
    //animate.css http://daneden.github.io/animate.css/
    ->registerCssFile($pt.'css/animate.css')
    //magic.css http://www.minimamente.com/example/magic_animations/
    ->registerCssFile($pt.'css/magic.css')
    //HZ slider
    ->registerCssFile($pt.'css/hz_slider.css')
    //arcticmodal
    ->registerCssFile($pt.'css/jquery.arcticmodal-0.3.css')
    ->registerCssFile($pt.'css/modal_simple.css')
       
    ->registerCssFile($pt.'css/main.css');

$cs
    
    ->registerCoreScript('jquery',CClientScript::POS_END)
    ->registerCoreScript('jquery.ui',CClientScript::POS_END)
    ->registerCoreScript('cookie',CClientScript::POS_END)
    //->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
    //->registerScriptFile($pt.'js/jquery-1.8.2.min.js',CClientScript::POS_END)
    //less 
    //->registerScriptFile($pt.'js/less.js',CClientScript::POS_END)
    //detected
    ->registerScriptFile($pt.'js/detect.min.js',CClientScript::POS_END)
    //slick slider
    ->registerScriptFile($pt.'js/slick.js',CClientScript::POS_END)
    //HZ slider
    ->registerScriptFile($pt.'js/caro_spizd/jquery.carouFredSel-6.2.1-packed.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/caro_spizd/category.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/caro_spizd/global.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/caro_spizd/jquery.infieldLabel.js',CClientScript::POS_END)
    //arcticmodal
    ->registerScriptFile($pt.'js/jquery.arcticmodal-0.3.min.js',CClientScript::POS_END)
        
    ->registerScriptFile($pt.'js/main.js',CClientScript::POS_END);

    $config=$this->config;
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?=Yii::app()->name; ?></title>
        <!--<link rel="stylesheet/less" type="text/css" href="/css/first.less">-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="ru">
        <meta name="robots" content="index, follow">
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/png">
    </head>
    <body>
        <script>
            // прелоадер начало
            $(document).ready(function () {
                setTimeout(function(){
                    $('#hellopreloader_preload').css('display','none');
                },5000);
                $('#loader-wrapper').animate({
                    opacity: 0
                }, 2000, null, function () {
                    $(this).hide();
                });

                $('.first_slider').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    dots: true
                });


            });
            // прелоадер конец 
        </script>
        <script>
            $(document).ready(function(){
                setTimeout(function(){
                    $('.products_title_box').each(function(){
                        var lenAll = ($(this).width()-(74*2)-(10*2)-$(this).find('.products_title').width()) ;
                        var lenLine = lenAll/2;
                        $(this).find('.products_left_line').width(lenLine);
                        $(this).find('.products_right_line').width(lenLine);
                    });
                },500);
                setTimeout(function(){
                    $('.main_about_company_title_top_box_line').each(function(){
                        var lenAll = ($(this).width()-(20*2)-$(this).find('.main_about_company_title_top_line_text').width()) ;
                        var lenLine = lenAll/2 - 1;
                        $(this).find('.main_about_company_title_top_line_left').width(lenLine);

                    });
                },500);
            });
        </script>
        <script>
            $(document).ready(function(){
                basket_refresh();
            });
        </script>
         <!-- HelloPreload http://hello-site.ru/preloader/ -->
        <style type="text/css">#hellopreloader>p{display:none;}#hellopreloader_preload{display: block;position: fixed;z-index: 99999;top: 0;left: 0;width: 100%;height: 100%;min-width: 1000px;background: #222222 url(http://www.efectivnutrition.com/skin/frontend/efectiv/default/images/e-loading-full-2.gif) center center no-repeat;background-size:73px;}</style>
        <div id="hellopreloader"><div id="hellopreloader_preload"></div><p><a href="http://hello-site.ru">Hello-Site.ru. Бесплатный конструктор сайтов.</a></p></div>
        <script type="text/javascript">var hellopreloader = document.getElementById("hellopreloader_preload");function fadeOutnojquery(el){el.style.opacity = 1;var interhellopreloader = setInterval(function(){el.style.opacity = el.style.opacity - 0.05;if (el.style.opacity <=0){ clearInterval(interhellopreloader);hellopreloader.style.display = "none";}},16);}window.onload = function(){setTimeout(function(){fadeOutnojquery(hellopreloader);},1000);};</script>
        <!-- HelloPreload http://hello-site.ru/preloader/ -->
        <!--<div id="loader-wrapper">
           <div id="loader2"></div>
        </div>-->
        
        
        <div class="header">
            <div class="container">
                <div class="header_box_top">
                    <a href="/">
                        <img class="logo" src="/img/logo.png"/>
                    </a>
                    <div class="header_contacts">
                        <div class="header_number">
                            <?= $config->phone ?>
                        </div>
                        <div class="header_email">

                        </div>
                    </div>
                    <a  href="/zakazat" class="header_cart_count">
                        0
                    </a>
                    <div class="header_cart_text">
                        выбрано <br/>товаров
                    </div>
                </div>
            </div>
            <div class="header_box_bot">
                <div class="container">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'id'=>'main_menu',
                        'items'=>array(
                            array('label'=>'О компании', 'url'=>array('site/main'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Продукты', 'url'=>array('site/products'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Заказать', 'url'=>array('site/toorder'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Контакты', 'url'=>array('site/contacts'),'itemOptions'=> array('class'=>'li_menu')),
                        ),
                        'htmlOptions'=>array(
                            'class'=>'',

                        ),
                        'activateParents'=>true,
                        'encodeLabel'=>false,
                    ));
                    ?>
                </div>
            </div>
        </div>

        <?= $content ?>

        <div class="footer_box">
            <div class="footer_box_top">
                <div class="container">
                    <div class="footer_social">
                        <a target="_blank" href="https://vk.com/efectiv_nutrition" class="soc_vk"></a>
                        <a target="_blank" href="https://www.facebook.com/groups/105216239814720/" class="soc_facebook"></a>
                        <a target="_blank" href="https://instagram.com/efectivru/" class="soc_instagramm"></a>
                    </div>
                </div>
            </div>
            <div class="footer_box_bot">
                <div class="container">
                    <div class="left_menu">
                        <?php
                        $this->widget('zii.widgets.CMenu', array(
                            'id'=>'footer_menu',
                            'items'=>array(
                                array('label'=>'О компании', 'url'=>array('/home'),'itemOptions'=> array('class'=>'li_menu')),
                                array('label'=>'Продукты', 'url'=>array('/produkti'),'itemOptions'=> array('class'=>'li_menu')),
                                array('label'=>'Заказать', 'url'=>array('/zakazat'),'itemOptions'=> array('class'=>'li_menu')),
                                array('label'=>'Контакты', 'url'=>array('/kontakti'),'itemOptions'=> array('class'=>'li_menu')),
                            ),
                            'htmlOptions'=>array(
                                'class'=>'',

                            ),
                            'activateParents'=>true,
                            'encodeLabel'=>false,
                        ));
                        ?>
                    </div>
                    <div class="right_copyright">
                        © EFECTIV Nutrition
                    </div>
                </div>
            </div>
        </div>

        <div style="display: none;">
            <?php 
                $yandex = Config::model()->findByPk(1);
                echo $yandex->yandex; 
            ?>
        </div>
                            <!-- модальные окна start -->

        <div style="display: none" id="goSelectTovar">
            <a onclick="$('#goModal_select_tovar').arcticmodal()"></a>
        </div>
        <div style="display: none;">
            <div class="box-modal bgr_go_contact" id="goModal_select_tovar">
                <div class="close_modal box-modal_close arcticmodal-close modal_close"></div>
                <div class="modal_title">Внимание!</div>
                <div class="modal_ty">для оформление заказа необходимо выбрать товар</div>
                <a  href="#" class="close_modal box-modal_close arcticmodal-close button_ok"></a>
            </div>
        </div>


        <div style="display: none" id="goGo">
            <a onclick="$('#goModal_what_next').arcticmodal()"></a>
        </div>
        <div style="display: none;">
            <div class="box-modal bgr_go_order" id="goModal_what_next">
                <div class="close_modal box-modal_close arcticmodal-close modal_close"></div>
                <div class="modal_title">Товар добавлен к заказу</div>
                <div class="modal_wn_box">
                    <a href="#" class="next_buy arcticmodal-close"></a>
                    <a href="/site/toorder" class="next_order"></a>
                </div>
            </div>
        </div>
        
        
        <div style="display: none" id="goSuccess">
            <a onclick="$('#goModal_ty').arcticmodal()"></a>
        </div>
        <div style="display: none;">
            <div class="box-modal bgr_go_contact" id="goModal_ty">
                <div class="close_modal box-modal_close arcticmodal-close modal_close"></div>
                <div class="modal_title">Спасибо, за заявку!</div>
                <div class="modal_ty">Наш менеджер свяжется с вами в ближайшее время</div>
                <a  href="#" class="close_modal box-modal_close arcticmodal-close button_ok"></a>
            </div>
        </div>
                            <!-- модальные окна end -->
    </body>
</html>