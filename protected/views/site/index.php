<?php
Yii::app()->name = $meta->meta_title;
Yii::app()->clientScript->registerMetaTag($meta->meta_description, 'description');
Yii::app()->clientScript->registerMetaTag($meta->meta_keywords, 'keywords');
?>
<script>
    $(document).ready(function(){
        $('ul.slick-dots').append('<div class="first_slider_dots_bgr">');
        $('div.first_slider_dots_bgr').append('<div class="first_slider_dots_bgr_dop">');
        $('div.first_slider_dots_bgr_dop').append('<div class="dots_bgr_left">');
        $('div.first_slider_dots_bgr_dop').append('<div class="dots_bgr_middle">');
        $('div.first_slider_dots_bgr_dop').append('<div class="dots_bgr_right">');
        var len = $('li[aria-hidden="true"]').length;
        console.log(len);
        var mid_width = (len+1)*24;
        console.log(mid_width);
        $('.dots_bgr_middle').width(mid_width);
        var first_slider_dots_bgr_dop_width = $('.dots_bgr_left').width() + mid_width + $('.dots_bgr_right').width();
        $('.first_slider_dots_bgr_dop').width(first_slider_dots_bgr_dop_width);
    });
</script>
<div class="new_slider_center">
    <div class="container container_for_slider">
        <div class="main_slider">
            <div class="first_slider">
                <a href="produkti?id=16"><img src="/img/banner_1.jpg" /></a>
                <!--<a href="#"><img src="/img/banner_2.jpg" /></a>-->
                <a href="produkti?id=14"><img src="/img/banner_3.jpg" /></a>
                <a href="produkti?id=25"><img src="/img/banner_4.jpg" /></a>
                <a href="produkti?id=20"><img src="/img/banner_5.jpg" /></a>
            </div>
        </div>
    </div>
</div>


<div class="main_about_company">
    <div class="main_about_company_bgr"></div>
    <div class="container ">
        <div class="main_about_company_head">
            <div class="main_about_company_title_top_box_line">
                <div class="main_about_company_title_top_line_left"></div>
                <div class="main_about_company_title_top_line_text">
                    EFECTIV NUTRION
                </div>
                <div class="main_about_company_title_top_line_left"></div>
            </div>
            <div class="main_about_company_title">
                О Компании
            </div>
            <div class="main_about_company_title_bot_line"></div>
        </div>
        <div class="main_about_company_content">
            <p>В компании EFECTIV мы стремимся сделать для Вас лучшие продукты премиум-класса, добиться идеальных вкусовых ощущений, мы используем только английские ингредиенты и весь цикл производства находится в Англии, даже упаковка EFECTIV.</p>
            <p>Наша единственная цель заключается в разработке продуктов, которые помогают Вам достигать поставленных результатов.</p>
            <p>EFECTIV – это Ваши инвестиции в свое здоровье, улучшение работоспособности и совершенство физической формы.</p>
            <p>Быть эффективным, TO BE EFFECTIV, - это в конечном итоге добиться цели, для которой Вы так упорно работали!</p>
            <p>Быть эффективным – это подход: использовать каждый день с пользой, дисциплинированно и целеустремленно идти к цели, получать обещанные преимущества от производителя спортивного питания.</p>
            <p>Сделай шаг к цели сегодня. Достигай больше - #будуэфектив!</p>
        </div>
        <img src="/img/main_about_tovar.png" class="main_about_company_tovar" />
    </div>
</div>
<div class="container">
    <div class="main_about_company_head_products margin_top_120">
        <div class="main_about_company_title_top_box_line">
            <div class="main_about_company_title_top_line_left"></div>
            <div class="main_about_company_title_top_line_text">
                EFECTIV NUTRION
            </div>
            <div class="main_about_company_title_top_line_left"></div>
        </div>
        <div class="main_about_company_title">
            Продукты EFECTIV
        </div>
        <div class="main_about_company_title_bot_line"></div>
    </div>
    <div class="main_about_company_content">
        <p>Приятно познакомиться! EFECTIV - Ваш надежный производитель спортивного питания премиум-класса по выгодным ценам. Мы используем лучшие английские ингредиенты, проверенные формулы и новейшие технологии получения приятных на вкус продуктов.</p>
        <p>Продукты EFECTIV помогут Вам выдержать интенсивные тренировки, быстро восстановить силы и добиться ожидаемых результатов.</p>
    </div>
    <div class="random_tovar_box">

        <?php
            $connection = Yii::app()->db;
            $command = $connection->createCommand(
                "SELECT `id`,`name`,`sub_name`,`preview` FROM `product` ORDER BY RAND() LIMIT 3"
            );
            $rows = $command->queryAll();

        ?>

        <?php foreach( $rows as $val ): ?>
            <a href="produkti?id=<?=$val['id']; ?>" class="link_product" >
                <div class="random_tovar">
                    <img class="random_tovar_img_left_right" src="/uploads/product/<?=$val['preview']; ?>"/>
                    <div class="random_tovar_text_box">
                        <div class="random_tovar_text_top ">
                            <?=$val['name']; ?>
                        </div>
                        <div class="random_tovar_text_bot">
                            <?=$val['sub_name'] ?>
                        </div>
                    </div>
                </div>
            </a>
        <?php endforeach; ?>



    </div>
    <a class="all_products" href="/produkti"></a>
</div>



<div class="container">
    <div class="main_footer_title_box">
        <div class="main_about_company_title_top_box_line">
            <div class="main_about_company_title_top_line_left"></div>
            <div class="main_about_company_title_top_line_text">
                ЕСТЬ ВОПРОСЫ?
            </div>
            <div class="main_about_company_title_top_line_left"></div>
        </div>
        <div class="main_about_company_title">
            Напишите нам!
        </div>
        <div class="main_about_company_title_bot_line"></div>
    </div>
    <div class="manager"></div>
    <div class="main_about_company_content">
        Если Вам нужно больше информации о продуктах EFECTIV или Вы хотите сделать заказ с доставкой в Ваш регион, начать выгодное сотрудничество с EFECTIV в России и Казахстане, команда EFECTIV будет счастлива помочь Вам или получить отзыв.
    </div>
    <div class="form_plz_box">
        <div class="form_plz_left"></div>
        <div class="form_plz_mid">ЗАПОЛНИТЕ ФОРМУ</div>
        <div class="form_plz_right"></div>
    </div>
    <div class="contact_form_box">
        <?php
        $orderForm = new OrderForm();
        $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
            'id'=>'order-form',
            'action'=>array('site/order'),
            'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
            'enableAjaxValidation'=>false,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange'=>false,
                'validateOnSubmit'=>true,
                'afterValidate' => "js: function(form, data, hasError) {
                    if ( !hasError) {
                        $.ajax({
                            type: 'POST',
                            url: $('#order-form').attr('action'),
                            data: $('#order-form').serialize(),
                            success: function(data_inner) {
                                if ( data_inner==1 ) {
                                
                                    $('#goSuccess a').click();
                                    $('#order-form').trigger('reset');
                                    
                                } else {
                                    alert('Хъюстон у нас проблемы!!!!');
                                }
                            }
                        });
                    }
                    return false;
                }
                ",
            ),
        ));  ?>


        <?php ?>
        <?php echo $form->errorSummary($orderForm); ?>

        <div class="row form_zakaz_name">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'name',array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_phone">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'phone',array('placeHolder' => 'Телефон', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_email">
            <?php echo $form->textField($orderForm,'email',array('placeHolder' => 'Email', 'required' => 'required')); ?>
        </div>
        
        <div class="row form_checkbox ziga">
            <?php echo $form->checkBox($orderForm,'subscr', array('checked'=>'checked')); ?>
            <label  for="subscr" class="form_checkbox_text">Я согласен получать новости EFECTIV</label>
        </div>

        <div class="row form_zakaz_commit">
            <?php echo $form->textArea($orderForm,'commit',array('placeHolder' => 'Коментарий','rows'=>8, 'cols'=>58), array()); ?>
        </div>

        <div class="required_must">
            *Обязательные поля
        </div>

        <div >
            <?= CHtml::submitButton('', array(
                'class'=>'button_zakaz',
            )) ?>
        </div>
        

        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="bgr_width">
    <div class="main_footer_bgr"></div>
</div>



