<?php
Yii::app()->name = $meta->meta_title;
Yii::app()->clientScript->registerMetaTag($meta->meta_description, 'description');
Yii::app()->clientScript->registerMetaTag($meta->meta_keywords, 'keywords');
?>

<div class="contacts_about_company">
    <div class="container">
        <div class="contacts_about_company_head">
            <div class="main_about_company_title_top_box_line">
                <div class="main_about_company_title_top_line_left"></div>
                <div class="main_about_company_title_top_line_text">
                    ЕСТЬ ВОПРОСЫ?
                </div>
                <div class="main_about_company_title_top_line_left"></div>
            </div>
            <div class="main_about_company_title">
                свяжитесь с нами
            </div>
            <div class="contacts_about_company_title_bot_line"></div>
        </div>
        <div class="main_about_company_content">
            Если Вам нужно больше информации о продуктах EFECTIV или Вы хотите сделать заказ с доставкой в Ваш регион, начать выгодное сотрудничество с EFECTIV в России и Казахстане, команда EFECTIV будет счастлива помочь Вам или получить отзыв.
        </div>
        
    </div>
</div>


<div class="container">
    <div class="contacts_box">
        <img class="ava" src="/img/ava_1.png"/>
        <div class="contacts_status">
            Генеральный директор компании
        </div>
        <div class="contacts_name">
            <span>Гаврик Игорь Вадимович</span>
        </div>
        <div class="contacts_phone">
            Телефон:  <span2>+79620368317</span2>
        </div>
        <div class="contacts_email">
            E-mail:  <span3><?= BsHtml::link('zakaz@efectiv.ru', 'mailto:zakaz@efectiv.ru', array('target'=>'_blank')) ?></span3>
        </div>
    </div>
</div>

<?php
/*
<div class="container">
    <div class="contacts_box">
        <img class="ava" src="/img/ava_2.png"/>
        <div class="contacts_status">
            Директор по продажам в РФ
        </div>
        <div class="contacts_name">
            <span>Чистякова Наталья Геннадьевна</span>
        </div>
        <div class="contacts_dop">
            (СФО, УРФО, ХМАО, ЯНАО, Республика Казахстан)
        </div>
        <div class="contacts_phone">
            Телефоны:  <span2>+79533921833,   +79835274392</span2>
        </div>
        <div class="contacts_email">
            E-mail:   <span3><?= BsHtml::link('zakaz@efectiv.ru', 'mailto:zakaz@efectiv.ru', array('target'=>'_blank')) ?></span3>
        </div>
    </div>
</div>
<div class="container">
    <div class="contacts_box">
        <img class="ava" src="/img/ava_3.png"/>
        <div class="contacts_status">
            Региональный Менеджер 
        </div>
        <div class="contacts_name">
            <span>Степико Константин Константинович</span>
        </div>
        <div class="contacts_dop">
            (ЦФО и ЮФО)
        </div>
        <div class="contacts_phone">          
            Телефоны:  <span2>+79620310205,   +79620577884  </span2>
        </div>
        <div class="contacts_email">
            E-mail:    <span3><?= BsHtml::link('zakaz@efectiv.ru', 'mailto:zakaz@efectiv.ru', array('target'=>'_blank')) ?></span3>
        </div>
    </div>
</div>
*/
?>



<div class="container">
    <div class="form_plz_box">
        <div class="form_plz_left"></div>
        <div class="form_plz_mid">ЗАПОЛНИТЕ ФОРМУ</div>
        <div class="form_plz_right"></div>
    </div>
    <div class="contact_form_box">
        <?php
        $orderForm = new OrderForm();
        $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
            'id'=>'order-form',
            'action'=>array('site/order'),
            'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
            'enableAjaxValidation'=>true,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange'=>false,
                'validateOnSubmit'=>true,
                'afterValidate' => "js: function(form, data, hasError) {
                    if ( !hasError) {
                        $.ajax({
                            type: 'POST',
                            url: $('#order-form').attr('action'),
                            data: $('#order-form').serialize(),
                            success: function(data_inner) {
                                if ( data_inner==1 ) {

                                    $('#goSuccess a').click();
                                    $('#order-form').trigger('reset');

                                } else {
                                    alert('Хъюстон у нас проблемы!!!!');
                                }
                            }
                        });
                    }
                    return false;
                }
                ",
            ),
        ));  ?>


        <?php ?>
        <?php echo $form->errorSummary($orderForm); ?>

        <div class="row form_zakaz_name">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'name',array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_phone">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'phone',array('placeHolder' => 'Телефон', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_email">
            <?php echo $form->textField($orderForm,'email',array('placeHolder' => 'Email', 'required' => 'required')); ?>
        </div>

        <div class="row form_checkbox ziga">
            <?php echo $form->checkBox($orderForm,'subscr', array('checked'=>'checked')); ?>
            <label  for="subscr" class="form_checkbox_text">Я согласен получать новости EFECTIV</label>
        </div>

        <div class="row form_zakaz_commit">
            <?php echo $form->textArea($orderForm,'commit',array('placeHolder' => 'Коментарий','rows'=>8, 'cols'=>58), array()); ?>
        </div>

        <div class="required_must">
            *Обязательные поля
        </div>

        <div >
            <?= CHtml::submitButton('', array(
                'class'=>'button_zakaz',
            )) ?>
        </div>


        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="bgr_width">
    <div class="contacts_footer_bgr"></div>
</div>