<?php
Yii::app()->name = $meta->meta_title;
Yii::app()->clientScript->registerMetaTag($meta->meta_description, 'description');
Yii::app()->clientScript->registerMetaTag($meta->meta_keywords, 'keywords');



$prod = $product_taste_volume->idProduct;
$tast = $product_taste_volume->idTaste;
$volum = $product_taste_volume->idVolume;
?>
<script>
    $(document).ready(function(){

        var topPos = $('.order_box_right').offset().top;
        $(window).scroll(function() {
            if($(window).width()>1000){
                var top = $(document).scrollTop(),
                    pip = $('.footer_box').offset().top,
                    height = $('.order_box_right').outerHeight();
                if (top > topPos && top < pip - height) {
                    $('.order_box_right').addClass('fixed').removeAttr("style");
                } else{
                    $('.order_box_right').removeClass('fixed');
                }
            }
        });



        /*$(window).scroll(function() {
            var top = $(document).scrollTop();
            if (top > 491) $('.order_box_right').addClass('fixed'); //200 - это значение высоты прокрутки страницы для добавления класс
            else $('.order_box_right').removeClass('fixed');
        });*/
    })
</script>
<script>
    $(document).ready(function(){
        var basket = JSON.parse($.cookie('basket'));
        $('.order_product_box').each(function(){
            for ( var i in basket){
                if (basket[i].id_product== $(this).attr('product_id') && basket[i].id_taste==$(this).attr('taste_id') && basket[i].id_volume==$(this).attr('volume_id')){
                    $(this).addClass('grey_joy');
                }
            }
            
        });
        
        $('.order_button_add').click(function(){
            
            basket_add($(this).parent().attr('product_id'), $(this).parent().attr('taste_id'), $(this).parent().attr('volume_id'), $(this).prev().find('select').val());
            
            var basket = JSON.parse($.cookie('basket'));
            for( var i in basket){
                if(basket[i].id_product== $(this).parent().attr('product_id') && basket[i].id_taste==$(this).parent().attr('taste_id') && basket[i].id_volume==$(this).parent().attr('volume_id')){
                     
                     
                      
                    if($('.order_product_dell[number_product="'+i+'"]').length!=0){
                        var html = '';
                        html += '<div class="img_micro_box"><img class="order_product_micro" src="'+$('.order_product_box[product_id="'+basket[i].id_product+'"]').find('.order_img_mini').attr('src')+'"/></div>';
                        html += '<div class="order_product_name">'+$('.order_product_box[product_id="'+basket[i].id_product+'"]').find('.order_name').html()+'</div>';
                        html += '<div class="order_product_volume">Объем: '+$(this).prev().prev().attr('volume_value')+'</div>';
                        html += '<div class="order_product_taste">Вкус: <span>'+$(this).prev().prev().prev().prev().prev().attr('taste_name')+'</span></div>';
                        html += '<div class="order_product_count_right">';
                        html += '<select name="count" class="this_right_count" number_product="'+i+'" id_product="'+basket[i].id_product+'" id_taste="'+basket[i].id_taste+'"" id_volume="'+basket[i].id_volume+'">';
                        for(var j =1; j<=99; j++ ){
                            if(j==basket[i].count){
                               html += '<option value="'+j+'" selected="selected" >'+j+'</option>' 
                            }
                            else{
                                html += '<option value="'+j+'" >'+j+'</option>' 
                            }
                        }
                        html += '</select>';
                        html += '</div>';
                        html += '<a href="#" number_product="'+i+'" id_product="'+basket[i].id_product+'" id_taste="'+basket[i].id_taste+'"" id_volume="'+basket[i].id_volume+'" class="order_product_dell"></a>';
                        
                        $('input[number_product="'+$(this).attr('number_product')+'"]').remove();
                        
                        $('.order_product_dell[number_product="'+i+'"]').parent().html(html);
                        
                        dell_order();
                        change_new_count();
                        
                        $('input[name="count[]"]').each(function(){
                           if($(this).attr('number_product')==i){
                               $(this).val(basket[i].count);
                           }
                        });
                    }else{
                        
                            
                        var html = '<div class="order_box_product_box">';
                        html += '<div class="img_micro_box"><img class="order_product_micro" src="'+$('.order_product_box[product_id="'+basket[i].id_product+'"]').find('.order_img_mini').attr('src')+'"/></div>';
                        html += '<div class="order_product_name">'+$('.order_product_box[product_id="'+basket[i].id_product+'"]').find('.order_name').html()+'</div>';
                        html += '<div class="order_product_volume">Объем: '+$(this).prev().prev().attr('volume_value')+'</div>';
                        html += '<div class="order_product_taste">Вкус: <span>'+$(this).prev().prev().prev().prev().prev().attr('taste_name')+'</span></div>';
                        html += '<div class="order_product_count_right">';
                        html += '<select name="count" class="this_right_count" number_product="'+i+'" id_product="'+basket[i].id_product+'" id_taste="'+basket[i].id_taste+'"" id_volume="'+basket[i].id_volume+'">';
                        for(var j =1; j<=99; j++ ){
                            if(j==basket[i].count){
                               html += '<option value="'+j+'" selected="selected" >'+j+'</option>' 
                            }
                            else{
                                html += '<option value="'+j+'" >'+j+'</option>' 
                            }
                        }
                        html += '</select>';
                        html += '</div>';
                        html += '<a href="#" number_product="'+i+'" id_product="'+basket[i].id_product+'" id_taste="'+basket[i].id_taste+'"" id_volume="'+basket[i].id_volume+'" class="order_product_dell"></a>';
                        html += '</div>';
                        var html2='';
                        html2 += '<input type="hidden" name="id_product[]" number_product="'+i+'" value="'+basket[i].id_product+'">';
                        html2 += '<input type="hidden" name="id_taste[]" number_product="'+i+'"value="'+basket[i].id_taste+'">';
                        html2 += '<input type="hidden" name="id_volume[]" number_product="'+i+'"value="'+basket[i].id_volume+'">';
                        html2 += '<input type="hidden" name="count[]" number_product="'+i+'" value="'+basket[i].count+'">';
                        $('.order_form_box').prepend(html);
                        $('#info_product').prepend(html2);

                    change_new_count();
                    dell_order();
                    }
                    
                    
                    
                }
            }

            
            
            return false;
            
        });
    })
</script>
<div class="order_about_company_head">
    <div class="main_about_company_title_top_box_line">
        <div class="main_about_company_title_top_line_left"></div>
        <div class="main_about_company_title_top_line_text">
            EFECTIV NUTRION
        </div>
        <div class="main_about_company_title_top_line_left"></div>
    </div>
    <div class="main_about_company_title">
        заказать
    </div>
    <div class="main_about_company_title_bot_line"></div>
</div>
<div class="main_about_company_content">
    EFECTIV - Ваш надежный производитель спортивного питания премиум-класса по выгодным ценам. Мы используем лучшие английские ингредиенты, проверенные формулы и новейшие технологии получения приятных на вкус продуктов. Продукты EFECTIV помогут Вам выдержать интенсивные тренировки, быстро восстановить силы и добиться ожидаемых результатов.
</div>

<div class="container min_height_950">
    
    <div class="order_box_left">

        <?php foreach ($product_taste_volume as $p_t_v): ?>
        <?php 
            $prod = $p_t_v->idProduct;
            $tast = $p_t_v->idTaste;
            $volum = $p_t_v->idVolume;
        ?>
        <div class="order_product_box" taste_id="<?= $tast->id ?>" volume_id="<?= $volum->id ?>" product_id="<?= $prod->id ?>" >
            <div class="order_product_box_bot_line"></div>
            <a href="/produkti?id=<?= $prod->id ?>" target="_blank" class="order_img_mini_box" >
                <img class="order_img_mini" src="/uploads/product/<?= $prod->preview?>"/>
            </a>
            <img  class="order_taste" taste_name="<?= $tast->name ?>" src="/uploads/taste/<?= $tast->preview?>">
            <a href="/produkti?id=<?= $prod->id ?>" target="_blank" class="order_name">
                <?= $prod->name?>
            </a>
            <div class="order_subname">
               <?= $prod->sub_name?>
            </div>
            <div class="order_v" volume_value="<?= $volum->value ?>">
               <?= $volum->value?>
            </div>
            <div class="order_product_count" >
                <?php
                    $number_drop = array();
                    for ($i = 1; $i <= 99; $i++) {
                        $number_drop[$i] = $i;
                    }
                ?>
                <?php echo CHtml::dropDownList('count', 1,
                    $number_drop
                );
                ?>
            </div>
            <a href="#" class="order_button_add" taste_id="<?= $tast->id ?>" volume_id="<?= $volum->id ?>" product_id="<?= $prod->id ?>" onclick="$(this).parent().addClass('grey_joy'); return false;"></a>
        </div>
        <?php endforeach; ?>
        
    </div>
    <div style="float: right; width: 494px;z-index: 0;">
        <div class="order_box_right">
            <div class="order_box_right_title">
                Ваш заказ
            </div>
            <script>
                $(document).ready(function(){
                    var basket = JSON.parse($.cookie('basket'));

                    var select = $('#count').html();
                    $("#my_select :nth-child(2)").attr("selected", "selected");
                    
                    
                    $('.order_button_add').each(function() {
                        for (var i in basket) {

                            if (basket[i].id_product == $(this).attr('product_id') && basket[i].id_taste == $(this).attr('taste_id') && basket[i].id_volume == $(this).attr('volume_id')) {


                            var html = '<div class="order_box_product_box">';
                            html += '<div class="img_micro_box"><img class="order_product_micro" src="' + $('.order_product_box[product_id="' + basket[i].id_product + '"]').find('.order_img_mini').attr('src') + '"/></div>';
                            html += '<div class="order_product_name">' + $('.order_product_box[product_id="' + basket[i].id_product + '"]').find('.order_name').html() + '</div>';
                                html += '<div class="order_product_volume">Объем: '+$(this).prev().prev().attr('volume_value')+'</div>';
                                html += '<div class="order_product_taste">Вкус: <span>'+$(this).prev().prev().prev().prev().prev().attr('taste_name')+'</span></div>';
                            html += '<div class="order_product_count_right">';
                            html += '<select name="count" class="this_right_count" number_product="' + i + '" id_product="' + basket[i].id_product + '" id_taste="' + basket[i].id_taste + '"" id_volume="' + basket[i].id_volume + '">';
                            for (var j = 1; j <= 99; j++) {
                                if (j == basket[i].count) {
                                    html += '<option value="' + j + '" selected="selected" >' + j + '</option>'
                                }
                                else {
                                    html += '<option value="' + j + '" >' + j + '</option>'
                                }
                            }
                            html += '</select>';
                            html += '</div>';
                            html += '<a href="#" number_product="' + i + '" id_product="' + basket[i].id_product + '" id_taste="' + basket[i].id_taste + '"" id_volume="' + basket[i].id_volume + '" class="order_product_dell"></a>';
                            html += '</div>';
                            var html2 = '';
                            html2 += '<input type="hidden" name="id_product[]" number_product="' + i + '" value="' + basket[i].id_product + '">';
                            html2 += '<input type="hidden" name="id_taste[]" number_product="' + i + '"value="' + basket[i].id_taste + '">';
                            html2 += '<input type="hidden" name="id_volume[]" number_product="' + i + '"value="' + basket[i].id_volume + '">';
                            html2 += '<input type="hidden" name="count[]" number_product="' + i + '" value="' + basket[i].count + '">';
                            $('.order_form_box').prepend(html);
                            $('#info_product').prepend(html2);
                            }
                            change_new_count();
                            dell_order();
                        }
                    });

                });
            </script>
            
            <div class="order_form_box">

                <script>
                    if( $('#info_product').html() != '' ){
                        var html2 = '';
                        html2 += '<input type="hidden" name="1" number_product="1" value="1">';
                        $('#info_product').prepend(html2);
                    }

                </script>

                <?php
                $orderForm = new OrderForm();
                $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                    'id'=>'order-form',
                    'action'=>array('site/order'),
                    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                    'enableAjaxValidation'=>true,
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnChange'=>false,
                        'validateOnSubmit'=>true,
                        'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#order-form').attr('action'),
                                data: $('#order-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {

                                        $('#goSuccess a').click();
                                        $.cookie('basket', null);
                                        $('.order_box_product_box').hide();
                                        $('#order-form').trigger('reset');
                                        basket_refresh()
                                    } else {
                                        alert('Введите номер телефона');
                                    }
                                }
                            });
                        }



                    return false;
                    }", ),
                ));  ?>


                <?php echo $form->errorSummary($orderForm); ?>

                <div class="row order_zakaz_name">
                    <?php echo $form->textField($orderForm,'name',array('placeHolder' => 'Ваше имя')); ?>
                </div>

                <div class="row order_zakaz_phone">
                    <?php echo $form->textField($orderForm,'phone',array('placeHolder' => 'Телефон', 'required' => 'required')); ?>
                </div>

                <div class="row order_zakaz_email">
                    <?php echo $form->textField($orderForm,'email',array('placeHolder' => 'Email')); ?>
                </div>

                <div class="row order_zakaz_face">
                    <?php echo $form->textField($orderForm,'face',array('placeHolder' => 'Наименование юридического лица')); ?>
                </div>

                <div class="row order_zakaz_commit">
                    <?php echo $form->textArea($orderForm,'commit',array('placeHolder' => 'Комментарий','rows'=>7, 'cols'=>48), array()); ?>
                </div>

                <div class="row order_checkbox_box ">
                    <?php echo $form->checkBox($orderForm,'subscr', array('checked'=>'checked')); ?>
                    <label  for="subscribe" class="form_checkbox_text">Я согласен получать новости EFECTIV</label>
                </div>

                <div id="info_product"></div>


                <div>
                    <?= CHtml::submitButton('', array(
                        'class'=>'button_order_zakaz',
                    )) ?>
                </div>


                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<div style="position: relative; width: 100%">
    <div class="order_bgr"></div>
</div>