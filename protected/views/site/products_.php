<?php
Yii::app()->name = $meta->meta_title;
Yii::app()->clientScript->registerMetaTag($meta->meta_description, 'description');
Yii::app()->clientScript->registerMetaTag($meta->meta_keywords, 'keywords');
?>

<div class="container">
    <div class="main_about_company_head">
        <div class="main_about_company_title_top_box_line">
            <div class="main_about_company_title_top_line_left"></div>
            <div class="main_about_company_title_top_line_text">
                EFECTIV NUTRION
            </div>
            <div class="main_about_company_title_top_line_left"></div>
        </div>
        <div class="main_about_company_title">
            Продукция
        </div>
        <div class="main_about_company_title_bot_line"></div>
    </div>
    <div class="main_about_company_content">
        EFECTIV - Ваш надежный производитель спортивного питания премиум-класса по выгодным ценам. Мы используем лучшие английские ингредиенты, проверенные формулы и новейшие технологии получения приятных на вкус продуктов. Продукты EFECTIV помогут Вам выдержать интенсивные тренировки, быстро восстановить силы и добиться ожидаемых результатов.
    </div>
</div>

<?php foreach($categories as $key=>$category): ?>
    <?php
        $products = $category->productCategory2;
        if(count($products) == 0){
            continue;
        }
    ?>
    <div class="container">
        <div class="products_title_box">
            <div class="products_left_line"></div>
            <div class="products_title">
                <?= $category->name ?>
            </div>
            <div class="products_right_line"></div>
        </div>
    </div>

    <div class="products_slider_box">
        <section id="products<?= $key+1 ?>" class="section-content">
            <div id="one-parallax<?= $key+1 ?>" class="parallax" >
                <span id="prev-prod<?= $key+1 ?>" class="large-arrow-btns"></span>
                <span id="next-prod<?= $key+1 ?>" class="large-arrow-btns"></span>
                <div class="home-products-wrap">
                    <ul class="home-products<?= $key+1 ?>">

                        <?php foreach( $products as $product ):?>
                            <li class="item last">
                                <a href="<?= $this->createUrl('/site/products',array('id'=>$product->nameProduct->id)) ?>" title="<?= $product->nameProduct->name?>" class="product-image"><img src="/uploads/product/<?= $product->nameProduct->preview?>" width="450" height="676" >
                                    <span class="product-name"><?= $product->nameProduct->name?></span>
                                    <span class="product-shrt-desc"><?= $product->nameProduct->sub_name?></span>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <!--<script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script>-->
                </div>
                <div class="clear"></div>
            </div>
        </section>
    </div>
<?php endforeach; ?>

    

<div class="container">
    <div class="main_footer_title_box">
        <div class="main_about_company_title_top_box_line">
            <div class="main_about_company_title_top_line_left"></div>
            <div class="main_about_company_title_top_line_text">
                ЕСТЬ ВОПРОСЫ?
            </div>
            <div class="main_about_company_title_top_line_left"></div>
        </div>
        <div class="main_about_company_title">
            НАПИШИТЕ НАМ!
        </div>
        <div class="main_about_company_title_bot_line"></div>
    </div>
    <div class="manager"></div>
    <div class="main_about_company_content">
        Если Вам нужно больше информации о продуктах EFECTIV или Вы хотите сделать заказ с доставкой в Ваш регион, начать выгодное сотрудничество с EFECTIV в России и Казахстане, команда EFECTIV будет счастлива помочь Вам или получить отзыв.
    </div>
    <div class="form_plz_box">
        <div class="form_plz_left"></div>
        <div class="form_plz_mid">ЗАПОЛНИТЕ ФОРМУ</div>
        <div class="form_plz_right"></div>
    </div>
    <div class="contact_form_box">
        <?php
        $orderForm = new OrderForm();
        $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
            'id'=>'order-form',
            'action'=>array('site/order'),
            'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
            'enableAjaxValidation'=>true,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange'=>false,
                'validateOnSubmit'=>true,
                'afterValidate' => "js: function(form, data, hasError) {
                    if ( !hasError) {
                        $.ajax({
                            type: 'POST',
                            url: $('#order-form').attr('action'),
                            data: $('#order-form').serialize(),
                            success: function(data_inner) {
                                if ( data_inner==1 ) {

                                    $('#goSuccess a').click();
                                    $('#order-form').trigger('reset');

                                } else {
                                    alert('Хъюстон у нас проблемы!!!!');
                                }
                            }
                        });
                    }
                    return false;
                }
                ",
            ),
        ));  ?>


        <?php ?>
        <?php echo $form->errorSummary($orderForm); ?>

        <div class="row form_zakaz_name">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'name',array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_phone">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'phone',array('placeHolder' => 'Телефон', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_email">
            <?php echo $form->textField($orderForm,'email',array('placeHolder' => 'Email', 'required' => 'required')); ?>
        </div>

        <div class="row form_checkbox ziga">
            <?php echo $form->checkBox($orderForm,'subscr', array('checked'=>'checked')); ?>
            <label  for="subscr" class="form_checkbox_text">Я согласен получать новости EFECTIV</label>
        </div>

        <div class="row form_zakaz_commit">
            <?php echo $form->textArea($orderForm,'commit',array('placeHolder' => 'Коментарий','rows'=>8, 'cols'=>58), array()); ?>
        </div>

        <div class="required_must">
            *Обязательные поля
        </div>

        <div >
            <?= CHtml::submitButton('', array(
                'class'=>'button_zakaz',
            )) ?>
        </div>


        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="bgr_width">
    <div class="products_footer_bgr"></div>
    <div class="products_footer_bgr_2"></div>
</div>