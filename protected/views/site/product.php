<?php
    Yii::app()->name = $meta->meta_title;
    Yii::app()->clientScript->registerMetaTag($meta->meta_description, 'description');
    Yii::app()->clientScript->registerMetaTag($meta->meta_keywords, 'keywords');

    $properties = $model->__properties;
    $promos = $model->promos;
    $tastes = $model->tastes;

?>
<script>
    $(document).ready(function(){
        $('#taste_id').change(function(){
            if($(this).val()==''){
                $('#volume_id').html('<option value="">Выберите объём</option>');
                return false;
            }
            $.ajax({
                url: '/site/_ajax?action=getVolumes',
                type: 'POST',
                dataType: 'json',
                data: {
                    id_product: <?= $model->id?>,
                    id_taste: $(this).val()
                },
                success: function(data) {
                    $('#volume_id').html('');
                    for ( var i in data ) {
                        $('#volume_id').append('<option value="'+data[i].id+'">'+data[i].value+'</option>');
                    }

                    if( i != 0 ){
                        $('#volume_id').prepend('<option value="">Выберите объём</option>');
                    }
                }
            });
        });
        $('.add_zakaz').click(function(){
            if($('#taste_id').val()!='' && $('#volume_id').val()!=''){
                basket_add(<?= $model->id?>, $('#taste_id').val(), $('#volume_id').val(), $('#count').val());
                $('#goGo a').click();
            } else {
                alert('Выберите все параметры');
            }
            return false;
        });
    });
</script>
<script>
    $(document).ready(function(){
        if ($('.map_product_promo').html() == '') {
            $('.map_product_promo').hide();
        }
        if ($('.box_info_product_text').html() == '') {
            $('.box_info_product_text').parent().hide();
        }
        if ($('.map_production_black_num').html() == '') {
            $('.map_production_black_num').parent().hide();
        }

    });
</script>
<div class="map_product_box">
    <div class="map_product_bgr"></div>
    <div class="container">
        <div class="map_product_image_box">
            <img  src="/uploads/product/<?= $model->preview ?>" class="map_product_image"/>
        </div>
        
        <div class="map_product_attributs">
            <div class="map_product_title_head">

                <div class="main_about_company_title_top_box_line">
                    <div class="main_about_company_title_top_line_left"></div>
                    <div class="main_about_company_title_top_line_text">
                        EFECTIV NUTRION
                    </div>
                    <div class="main_about_company_title_top_line_left"></div>
                </div>
                <div class="map_category_text_title">
                    <?= $model->name ?>
                </div>
                <div class="main_about_company_title_bot_line"></div>
            </div>
            <div class="map_product_promo"><?= $model->sub_name ?></div>
            <div class="map_product_eco_box">
                <?php foreach($promos as $promo ): ?>
                    <img src="/uploads/promo/<?= $promo->preview ?>">
                <?php endforeach ?>
            </div>

            <?php foreach($properties as $pro): ?>
                <div class="map_product_info_box">
                    <?php if ($pro!==''){
                        echo '<span>+ &nbsp;</span>'.$pro;
                    } ?>
                </div>
            <?php endforeach; ?>
            <script>
                $(document).ready(function(){
                    setTimeout(function(){
                        var len1 = $('.map_production_information_div:eq(0)').width();
                        var len2 = $('.map_production_information_div:eq(1)').width();
                        var len3 = $('.map_production_information_div:eq(2)').width();
                        var len = len1 + len2 + len3;
                        $('.map_product_information_box').width(len + 10);
                        $('.orange_black_box').css('width','100%');
                        if(len>534){
                            var marg = 534-len;
                            $('.map_product_information_box').css('margin-left',marg);
                        }


                    },500);
                });
            </script>

            <div class="map_product_information_box">
                <div class="orange_black_box">
                    <?php if ( $model->black_1!=='' && $model->orange_1!=='' ){
                        echo '<div class="map_production_information_div">';
                        echo'<div class="map_production_black_num">';
                        echo $model->black_1;
                        echo '</div>';
                        echo'<div class="map_production_orange_num">';
                        echo $model->orange_1;
                        echo '</div>';
                        echo '</div>';
                    } ?>

                    <?php if ( $model->black_2!=='' && $model->orange_2!=='' ){
                        echo '<div class="map_production_information_div">';
                        echo'<div class="map_production_black_num">';
                        echo $model->black_2;
                        echo '</div>';
                        echo'<div class="map_production_orange_num">';
                        echo $model->orange_2;
                        echo '</div>';
                        echo '</div>';
                    } ?>
                    <?php if ( $model->black_3!=='' && $model->orange_3!=='' ){
                        echo '<div class="map_production_information_div">';
                        echo'<div class="map_production_black_num">';
                        echo $model->black_3;
                        echo '</div>';
                        echo'<div class="map_production_orange_num">';
                        echo $model->orange_3;
                        echo '</div>';
                        echo '</div>';
                    } ?>
                </div>

            </div>
        </div>
        <div class="map_product_dropdown_box">
            <div class="map_product_taste">
                <?php
                if( count($tastes) == 1 ){
                    foreach( $tastes as $val ){
                        $taste_id = $val->id;
                    };
                ?>

                    <script>
                        $(document).ready(function(){
                            $.ajax({
                                url: '/site/_ajax?action=getVolumes',
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    id_product: <?= $model->id?>,
                                    id_taste: <?=$taste_id; ?>
                                },
                                success: function(data) {

                                    for ( var i in data ) {
                                        $('#volume_id').append('<option value="'+data[i].id+'">'+data[i].value+'</option>');
                                    }
                                    $('.map_product_taste').css('opacity',0);
                                }
                            });
                        });
                        $('.add_zakaz').click(function(){
                            if($('#taste_id').val()!='' && $('#volume_id').val()!=''){
                                basket_add(<?= $model->id?>, $('#taste_id').val(), $('#volume_id').val(), $('#count').val());
                                $('#goGo a').click();
                            } else {
                                alert('Выберите все параметры');
                            }
                            return false;
                        });

                    </script>
                    <?php echo CHtml::dropDownList('taste_id', null,
                        CHtml::listData($tastes,'id','name'),
                        array('243' => 'Выбрать вкус')
                    );
                    ?>
                <?php }else{ ?>

                    <?php
                    $taste_list = array(array('id'=>0,'name'=>'Выбрать вкус'));
                    $mass = $tastes;
                    $count=1;
                    foreach( $mass as $val ){
                        $taste_list[$count]['id'] = $val->id;
                        $taste_list[$count]['name'] = mb_strtoupper($val->name, 'UTF-8');
                        $count++;
                    }
                    echo BsHtml::dropDownList('taste_id', null, BsHtml::listData($taste_list, 'id','name'));

                    ?>

                <?php } ?>

            </div>


            <?php if( count($tastes) == 1 ){ ?>
                <div class="map_product_v">
                    <?php echo CHtml::dropDownList('volume_id', null,
                        array()
                    );
                    ?>
                </div>
            <?php } else { ?>
                <div class="map_product_v">
                    <?php echo CHtml::dropDownList('volume_id', null,
                        array(),
                        array('empty' => 'Выбрать объём')
                    );
                    ?>
                </div>
            <?php } ?>

            <div class="map_product_black_box">
                <div class="map_product_count">
                    <?php
                        $number_drop = array();
                        for ($i = 1; $i <= 99; $i++) {
                            $number_drop[$i] = $i;
                        }
                    ?>
                    <?php echo CHtml::dropDownList('count', 1,
                        $number_drop
                    );
                    ?>
                </div>
                <a href="#" class="add_zakaz" ></a>
            </div>

        </div>
        <div class="map_product_taste_box">
            <?php foreach ($tastes as $taste):?>
                <img src="/uploads/taste/<?= $taste->preview ?>">
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php if ( $model->description != '' && $model->nutritional_value != '' ):?>
    <div class="map_product_box_info_bgr">
        <div class="container">
            <?php if( $model->nutritional_value != '' ): ?>
                <div class="box_info_product_left">
                    <div class="box_info_product_title">
                        пищевая ценность
                    </div>
                    <div class="box_info_product_text">
                        <?= $model->nutritional_value ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ( $model->description != ''): ?>
            <div class="box_info_product_right">
                <div class="box_info_product_title">
                    описание
                </div>
                <div class="box_info_product_text">
                    <?= $model->description ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<div class="container">
    <div class="map_product_contact_title_head">
        <div class="main_about_company_title_top_box_line">
            <div class="main_about_company_title_top_line_left"></div>
            <div class="main_about_company_title_top_line_text">
                ЕСТЬ ВОПРОСЫ?
            </div>
            <div class="main_about_company_title_top_line_left"></div>
        </div>
        <div class="main_about_company_title">
            Напишите нам!
        </div>
        <div class="main_about_company_title_bot_line"></div>
    </div>
    <div class="manager"></div>
    <div class="main_about_company_content">
        Если Вам нужно больше информации о продуктах EFECTIV или Вы хотите сделать заказ с доставкой в Ваш регион, начать выгодное сотрудничество с EFECTIV в России и Казахстане, команда EFECTIV будет счастлива помочь Вам или получить отзыв.
    </div>
    <div class="form_plz_box">
        <div class="form_plz_left"></div>
        <div class="form_plz_mid">ЗАПОЛНИТЕ ФОРМУ</div>
        <div class="form_plz_right"></div>
    </div>
    <div class="contact_form_box">
        <?php
        $orderForm = new OrderForm();
        $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
            'id'=>'order-form',
            'action'=>array('site/order'),
            'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
            'enableAjaxValidation'=>false,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange'=>false,
                'validateOnSubmit'=>true,
                'afterValidate' => "js: function(form, data, hasError) {
                    if ( !hasError) {
                        $.ajax({
                            type: 'POST',
                            url: $('#order-form').attr('action'),
                            data: $('#order-form').serialize(),
                            success: function(data_inner) {
                                if ( data_inner==1 ) {

                                    $('#goSuccess a').click();
                                    $('#order-form').trigger('reset');

                                } else {
                                    alert('Хъюстон у нас проблемы!!!!');
                                }
                            }
                        });
                    }
                    return false;
                }
                ",
            ),
        ));  ?>


        <?php ?>
        <?php echo $form->errorSummary($orderForm); ?>

        <div class="row form_zakaz_name">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'name',array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_phone">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'phone',array('placeHolder' => 'Телефон', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_email">
            <?php echo $form->textField($orderForm,'email',array('placeHolder' => 'Email', 'required' => 'required')); ?>
        </div>

        <div class="row form_checkbox ziga">
            <?php echo $form->checkBox($orderForm,'subscr', array('checked'=>'checked')); ?>
            <label  for="subscr" class="form_checkbox_text">Я согласен получать новости EFECTIV</label>
        </div>

        <div class="row form_zakaz_commit">
            <?php echo $form->textArea($orderForm,'commit',array('placeHolder' => 'Коментарий','rows'=>8, 'cols'=>58), array()); ?>
        </div>

        <div class="required_must">
            *Обязательные поля
        </div>

        <div>
            <?= CHtml::submitButton('', array(
                'class'=>'button_zakaz',
            )) ?>
        </div>


        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="bgr_width">
    <div class="map_product_footer_bgr"></div>
</div>