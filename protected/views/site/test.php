<section id="products" class="section-content">
    <div id="one-parallax" class="parallax" >

        <div class="page-title category-title section-title">
            <h1>Recovery</h1>
        </div>
        <span id="prev-prod" class="large-arrow-btns"></span>
        <span id="next-prod" class="large-arrow-btns"></span>
        <div class="home-products-wrap">
            <ul class="home-products">

                <li class="item last">
                    <a href="http://www.efectivnutrition.com/efectiv-amino-injection-matrix.html" title="EFECTIV Amino Injection Matrix 420g " class="product-image"><img src="http://www.efectivnutrition.com/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/e/f/efectiv-nutrition-aim_3.png" width="450" height="676" alt="EFECTIV Amino Injection Matrix 420g ">
                        <span class="product-name">EFECTIV AIM</span>
                        <span class="product-shrt-desc">Instantised BCAA Delivery System with 12g Amino Acids</span>



                        <div class="price-box">
                                                                <span class="regular-price" id="product-price-5">
                                            <span class="price">?39.99</span>                                    </span>

                        </div>

                    </a>
                </li>


                <li class="item last">
                    <a href="http://www.efectivnutrition.com/efectiv-glutamine-plus.html" title="EFECTIV Glutamine Plus 400g Unflavoured" class="product-image"><img src="http://www.efectivnutrition.com/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/e/f/efectiv-nutrition-glutamine_2.png" width="450" height="676" alt="EFECTIV Glutamine Plus 400g Unflavoured">
                        <span class="product-name">EFECTIV Glutamine</span>
                        <span class="product-shrt-desc">Ultra-pure, pharmaceutical grade L-Glutamine powder</span>



                        <div class="price-box">
                                                                <span class="regular-price" id="product-price-9">
                                            <span class="price">?24.99</span>                                    </span>

                        </div>

                    </a>
                </li>


                <li class="item last">
                    <a href="http://www.efectivnutrition.com/efectiv-whey.html" title="EFECTIV Whey 2kg" class="product-image"><img src="http://www.efectivnutrition.com/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/e/f/efectiv-nutrition-whey.png" width="450" height="676" alt="EFECTIV Whey 2kg">
                        <span class="product-name">EFECTIV Whey</span>
                        <span class="product-shrt-desc">Advanced protein complex to support lean muscle growth</span>



                        <div class="price-box">
                                                                <span class="regular-price" id="product-price-17">
                                            <span class="price">?49.99</span>                                    </span>

                        </div>

                    </a>
                </li>


                <li class="item last">
                    <a href="http://www.efectivnutrition.com/zm-pro-90-capsules.html" title="ZM PRO 90 capsules - Efectiv Nutrition" class="product-image"><img src="http://www.efectivnutrition.com/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/e/f/efectiv-nutrition-zm-pro.png" width="450" height="676" alt="ZM PRO 90 capsules - Efectiv Nutrition">
                        <span class="product-name">ZM PRO</span>
                        <span class="product-shrt-desc">Optimised male formula</span>



                        <div class="price-box">
                                                                <span class="regular-price" id="product-price-21">
                                            <span class="price">?12.99</span>                                    </span>

                        </div>

                    </a>
                </li>

            </ul>

            <script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script>


        </div>


        <div class="clear"></div>
    </div>
</section>