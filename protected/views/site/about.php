<?php
Yii::app()->name = $meta->meta_title;
Yii::app()->clientScript->registerMetaTag($meta->meta_description, 'description');
Yii::app()->clientScript->registerMetaTag($meta->meta_keywords, 'keywords');
?>
<script>
    $(document).ready( function(){
        var video =  document.getElementById(element);
        video.addEventListener('click',function(){
            video.play();
        },false);
    });
</script>
<div class=".main_about_company_aboutPage">
    <div class="container">
        <div class="main_about_company_head">
            <div class="main_about_company_title_top_box_line">
                <div class="main_about_company_title_top_line_left"></div>
                <div class="main_about_company_title_top_line_text">
                    EFECTIV NUTRION
                </div>
                <div class="main_about_company_title_top_line_left"></div>
            </div>
            <div class="main_about_company_title">
                О Компании
            </div>
            <div class="main_about_company_title_bot_line"></div>
        </div>
        <div class="main_about_company_content">
            В компании EFECTIV мы стремимся сделать для Вас лучшие продукты премиум-класса, добиться идеальных вкусовых ощущений, мы используем только английские ингредиенты и все цикл производства находится в Англии, даже упаковка EFECTIV.
            Наша единственная цель заключается в разработке продуктов, которые помогают Вам достигать поставленных результатов, EFECTIV – это Ваши инвестиции в свое здоровье, улучшение работоспособности и совершенство физической формы.
            Быть эффективным, TO BE EFFECTIV, - это в конечном итоге добиться цели, для которой Вы так упорно работали! Быть эффективным – это подход: использовать каждый день с пользой, дисциплинированно и целеустремленно идти к цели, получать обещанные преимущества от производителя спортивного питания.
            Сделай шаг к цели сегодня. Достигай больше - #будуэфектив!
        </div>
    </div>
</div>
<!--
<div id="trailer" class="is_overlay">
    <div class="container">
        <video autoplay="autoplay" controls  poster="/img/poster_video.png"' id="video" width="100%" height="auto"  >
            <source src="/img/video.mp4"></source>
            <source src="/img/video.webm"></source>
        </video>
    </div>
</div>-->