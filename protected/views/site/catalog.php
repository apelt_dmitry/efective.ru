<?php
Yii::app()->name = $meta->meta_title;
Yii::app()->clientScript->registerMetaTag($meta->meta_description, 'description');
Yii::app()->clientScript->registerMetaTag($meta->meta_keywords, 'keywords');
?>
<script>
    $(document).ready(function(){
        $('ul.slick-dots').append('<div class="first_slider_dots_bgr">');
        $('div.first_slider_dots_bgr').append('<div class="first_slider_dots_bgr_dop">');
        $('div.first_slider_dots_bgr_dop').append('<div class="dots_bgr_left">');
        $('div.first_slider_dots_bgr_dop').append('<div class="dots_bgr_middle">');
        $('div.first_slider_dots_bgr_dop').append('<div class="dots_bgr_right">');
        var len = $('li[aria-hidden="true"]').length;
        console.log(len);
        var mid_width = (len+1)*24;
        console.log(mid_width);
        $('.dots_bgr_middle').width(mid_width);
        var first_slider_dots_bgr_dop_width = $('.dots_bgr_left').width() + mid_width + $('.dots_bgr_right').width();
        $('.first_slider_dots_bgr_dop').width(first_slider_dots_bgr_dop_width);

        $('.home-products').carouFredSel({
            align: "center",
            responsive: false,
            circular: true,
            auto: false,
            infinite: true,
            width: '100%',
            items: {
                visible: 3
            },
            scroll : {
                duration: 1000,
                items: 1
            },
            next : {
                button: "#next-prod",
                key: "right"
            },
            prev : {
                button: "#prev-prod",
                key: "left"
            }
        });
    });
</script>
<div class="main_slider">
    <div class="first_slider">
        <a href="#"><img src="/img/banner_1.jpg" /></a>
        <a href="#"><img src="/img/banner_2.jpg" /></a>
        <a href="#"><img src="/img/banner_3.jpg" /></a>
        <a href="#"><img src="/img/banner_4.jpg" /></a>
        <a href="#"><img src="/img/banner_5.jpg" /></a>
    </div>
</div>
<div class="main_about_company">
    <div class="main_about_company_bgr"></div>
    <div class="container">
        <div class="main_about_company_head">
            <div class="main_about_company_title_top_box_line">
                <div class="main_about_company_title_top_line"></div>
                <div class="main_about_company_title_top_line_text">
                    EFECTIV NUTRITION
                </div>
            </div>
            <div class="main_about_company_title">
                О Компании
            </div>
            <div class="main_about_company_title_bot_line"></div>
        </div>
        <div class="main_about_company_content">
            Разнообразный и богатый опыт новая модель организационной деятельности способствует подготовки и реализации существенных финансовых и административных условий. Значимость этих проблем настолько очевидна, что укрепление и развитие структуры представляет собой интересный эксперимент проверки существенных финансовых и административных условий. Повседневная практика показывает, что дальнейшее развитие различных форм деятельности обеспечивает широкому кругу участие в формировании существенных финансовых и административных условий.
        </div>
        <img src="/img/main_about_tovar.png" class="main_about_company_tovar" />
    </div>
</div>
<div class="container">
    <div class="main_about_company_head">
        <div class="main_about_company_title_top_box_line">
            <div class="main_about_company_title_top_line"></div>
            <div class="main_about_company_title_top_line_text bgr_white">
                EFECTIV NUTRITION
            </div>
        </div>
        <div class="main_about_company_title">
            Продукция
        </div>
        <div class="main_about_company_title_bot_line"></div>
    </div>
    <div class="main_about_company_content">
        Товарищи! консультация с широким активом требуют определения и уточнения соответствующий условий активизации. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки дальнейших направлений развития.
    </div>
    <div class="random_tovar_box">
        <div class="random_tovar">
            <img class="random_tovar_img_left_right" src="/img/tovar_3.png"/>
            <div class="random_tovar_text_box">
                <div class="random_tovar_text_top ">
                    КАЗЕИН EFECTIV
                </div>
                <div class="random_tovar_text_bot">
                    100% МИЦЕЛЛЯРНЫЙ КАЗЕИН
                </div>
            </div>
        </div>
        <div class="random_tovar">
            <img class="random_tovar_img_mid" src="/img/tovar_1.png"/>
            <div class="random_tovar_text_box">
                <div class="random_tovar_text_top">
                    СЫВОРОТКА EFECTIV
                </div>
                <div class="random_tovar_text_bot">
                    УЛУЧШЕННЫЙ ПРОТЕИНОВЫЙ КОМПЛЕКС
                </div>
            </div>
        </div>
        <div class="random_tovar">
            <img class="random_tovar_img_left_right" src="/img/tovar_2.png"/>
            <div class="random_tovar_text_box">
                <div class="random_tovar_text_top">
                    ГЛЮТАМИН ПЛЮС
                </div>
                <div class="random_tovar_text_bot">
                    ФАРМАЦЕВТИЧЕСКИЙ КЛАСС
                </div>
            </div>
        </div>

    </div>
    <a class="all_products" href="#"></a>
</div>
</div>


<div class="container">
    <div class="main_footer_title_box">
        <div class="main_about_company_title_top_box_line">
            <div class="main_about_company_title_top_line"></div>
            <div class="main_about_company_title_top_line_text bgr_white">
                ЕСТЬ ВОПРОСЫ?
            </div>
        </div>
        <div class="main_about_company_title">
            свяжитесь с нами
        </div>
        <div class="main_about_company_title_bot_line"></div>
    </div>
    <div class="manager"></div>
    <div class="main_about_company_content">
        Повседневная практика показывает, что реализация намеченных плановых заданий требуют определения и уточнения новых предложений. Значимость этих проблем настолько очевидна, что начало повседневной работы по формированию позиции влечет за собой процесс внедрения и модернизации форм развития.
    </div>
    <div class="form_plz_box">
        <div class="form_plz_left"></div>
        <div class="form_plz_mid">ЗАПОЛНИТЕ ФОРМУ</div>
        <div class="form_plz_right"></div>
    </div>
    <div class="contact_form_box">
        <?php
        $orderForm = new OrderForm();
        $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
            'id'=>'order-form',
            'action'=>array('site/order'),
            'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
            'enableAjaxValidation'=>false,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnChange'=>false,
                'validateOnSubmit'=>true,
                'afterValidate' => "js: function(form, data, hasError) {
                    if ( !hasError) {
                        $.ajax({
                            type: 'POST',
                            url: $('#order-form').attr('action'),
                            data: $('#order-form').serialize(),
                            success: function(data_inner) {
                                if ( data_inner==1 ) {

                                    $('#goSuccess a').click();

                                } else {
                                    alert('Хъюстон у нас проблемы!!!!');
                                }
                            }
                        });
                    }
                    return false;
                }
                ",
            ),
        ));  ?>


        <?php ?>
        <?php echo $form->errorSummary($orderForm); ?>

        <div class="row form_zakaz_name">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'name',array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_phone">
            <img src="/img/zvezda.png" class="zvezda" />
            <?php echo $form->textField($orderForm,'phone',array('placeHolder' => 'Телефон', 'required' => 'required')); ?>
        </div>

        <div class="row form_zakaz_email">
            <?php echo $form->textField($orderForm,'email',array('placeHolder' => 'Email', 'required' => 'required')); ?>
        </div>

        <div class="row form_checkbox ziga">
            <?php echo $form->checkBox($orderForm,'subscr', array('checked'=>'checked')); ?>
            <label  for="subscr" class="form_checkbox_text">Я согласен получать новости товара</label>
        </div>

        <div class="row form_zakaz_commit">
            <?php echo $form->textArea($orderForm,'commit',array('placeHolder' => 'Коментарий','rows'=>8, 'cols'=>58), array()); ?>
        </div>

        <div class="required_must">
            *Обязательные поля
        </div>

        <div >
            <?= CHtml::submitButton('', array(
                'class'=>'button_zakaz',
            )) ?>
        </div>


        <?php $this->endWidget(); ?>
    </div>
</div>
<div style="position: relative; width: 100%">
    <div class="main_footer_bgr"></div>
</div>

<section id="products" class="section-content">
    <div id="one-parallax" class="parallax">
        <span id="prev-prod" class="large-arrow-btns" style="display: block;"></span>
        <span id="next-prod" class="large-arrow-btns" style="display: block;"></span>
        <div class="home-products-wrap">
            <div class="caroufredsel_wrapper" style="display: block; text-align: center; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 1903px; height: 510px; margin: 0px 0px 10px; overflow: hidden;"><ul class="home-products" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 502px; margin: 0px; width: 4303px; height: 510px; z-index: auto;">
                    <li class="item last" style="margin-right: 0px;">
                        <a href="#" title="EFECTIV Whey 2kg" class="product-image">
                            <img src="/img/tovar_1.png" width="450" height="676" >
                        </a>
                    </li>
                    <li class="item last" style="margin-right: 0px;">
                        <a href="#" title="ZM PRO 90 capsules - Efectiv Nutrition" class="product-image">
                            <img src="/img/tovar_2.png.png" width="450" height="676" >
                        </a>
                    </li>
                    <li class="item last" style="margin-right: 501px;">
                        <a href="#" title="EFECTIV Amino Injection Matrix 420g " class="product-image">
                            <img src="/img/tovar_3.png" width="450" height="676" >
                        </a>
                    </li><li class="item last" style="margin-right: 0px;">
                        <a href="#" title="EFECTIV Glutamine Plus 400g Unflavoured" class="product-image">
                            <img src="/img/tovar_1.png" width="450" height="676" >
                        </a>
                    </li>
                </ul>
            </div>
            <!--<script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script>-->
        </div>
        <div class="clear"></div>
    </div>
</section>

<div id="wrapper">
    <span id="prev-prod" class="large-arrow-btns" style="display: block;"></span>
    <span id="next-prod" class="large-arrow-btns" style="display: block;"></span>
    <div id="images">
        <div class="carousel">
            <img src="img/tovar_1.png" alt="cod" width="700" height="350" />
            <img src="img/tovar_1.png" alt="cod" width="700" height="350" />
            <img src="img/tovar_1.png" alt="cod" width="700" height="350" />
            <img src="img/tovar_1.png" alt="gta" width="700" height="350" />
            <img src="img/tovar_1.png" alt="gta" width="700" height="350" />
            <img src="img/tovar_1.png" alt="gta" width="700" height="350" />
        </div>
    </div>
    <div id="timer"></div>
    <div id="captions">
        <div class="carousel">
            <div class="cod">Call Of Duty
                <div class="pager"></div>
            </div>
            <div class="gta">Grand Theft Auto
                <div class="pager"></div>
            </div>
            <div class="mgs">Metal Gear Solid
                <div class="pager"></div>
            </div>
        </div>
    </div>
</div>



