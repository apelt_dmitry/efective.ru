// Cache selectors
var lastId,
    topMenu = $("#top-menu"),
    topMenuHeight = topMenu.outerHeight()+15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    }),
    noScrollAction = false;

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    noScrollAction = true;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    },{
        duration: 300,
        complete: function() {
            menuItems
                .parent().removeClass("active")
                .end().filter("[href=" + href +"]").parent().addClass("active");
            setTimeout(function(){ noScrollAction = false; }, 10);
        }
    });
    e.preventDefault();
});

// Bind to scroll
$(window).scroll(function(){
   if(!noScrollAction){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
       
       if (lastId !== id) {
           lastId = id;
           // Set/remove active class
           menuItems
             .parent().removeClass("active")
             .end().filter("[href=#"+id+"]").parent().addClass("active");
       }
   }    
});

$(document).ready(function(){
    
    $('[animate]').css('opacity','0');
    $('.reviews_box').css('opacity','0');
    $('.container > *').attr('animate','a')
    animatecss();
    magiccss();
});
$(window).scroll(function(){
    animatecss();
    magiccss();
    otziv();
});
function animatecss()
{
    $('[animate]').each(function(){  
        if ( !$(this).hasClass('active') ) {
            if ( $(window).scrollTop()+$(window).height()>$(this).offset().top+200 ) {
                $(this).addClass('active');
                $(this).addClass('animated '+$(this).attr('animate'));
            }
        }
        //else {
           // if ( $(window).scrollTop()+$(this).height()<$(this).offset().top ) {
           //     $(this).removeClass('active');
            //    $(this).removeClass('animated '+$(this).attr('animate'));
        //    }
        //}
    });    
}
function magiccss()
{
    $('[animate]').each(function(){  
        if ( !$(this).hasClass('active') ) {
            if ( $(window).scrollTop()+$(window).height()>$(this).offset().top ) {
                $(this).addClass('active');
                $(this).addClass('magictime '+$(this).attr('animate'));
            }
        }
        //else {
           // if ( $(window).scrollTop()+$(this).height()<$(this).offset().top ) {
           //     $(this).removeClass('active');
            //    $(this).removeClass('animated '+$(this).attr('animate'));
        //    }
        //}
    });    
}
function otziv(){
    $('.reviews_box').each(function(){  
        if ( !$(this).hasClass('act') ) {
            if ( $(window).scrollTop()+$(window).height()>$(this).offset().top + 250 ) {
                $(this).css('opacity','1');
                $(this).addClass('element-animation');
                $(this).addClass('act');
                
            }
        }
        //else {
           // if ( $(window).scrollTop()+$(this).height()<$(this).offset().top ) {
           //     $(this).removeClass('active');
            //    $(this).removeClass('animated '+$(this).attr('animate'));
        //    }
        //}
    });  
}
function basket_add(id_product,id_taste,id_volume,count){
    var basket = JSON.parse($.cookie('basket'));
    if(basket===null){
        basket = [];
        basket.push({
            id_product: id_product,
            id_taste: id_taste,
            id_volume: id_volume,
            count: count
        });
        $.cookie('basket', JSON.stringify(basket) , { path: '/', expires: 30 });
    } else{
        var nashli = false;
        for( var i in basket){
            if(basket[i].id_product== id_product && basket[i].id_taste==id_taste && basket[i].id_volume==id_volume){
                basket[i].count = basket[i].count*1+count*1;
                nashli = true;
            }
        }
        if (!nashli){
            basket.push({
                id_product: id_product,
                id_taste: id_taste,
                id_volume: id_volume,
                count: count
            });
        }
        $.cookie('basket', JSON.stringify(basket) , { path: '/', expires: 30 });
    }
                                    //вызываем модальное окно
    basket_refresh()
}
function basket_refresh(){
    //$('.header_cart_count').text(JSON.parse($.cookie('basket')).length);
    var basket = JSON.parse($.cookie('basket'));
    var count = 0;
    for(var i in basket){
        count = count*1 + basket[i].count*1;
    }
    $('.header_cart_count').text(count);
}
function dell_order(){
    
    $('.order_product_dell').click(function(){
        //if(confirm('Удалить товар?')){
            var basket = JSON.parse($.cookie('basket'));
            var basket2 = [];
            for(var i in basket){
                if (basket[i].id_product== $(this).attr('id_product') && basket[i].id_taste==$(this).attr('id_taste') && basket[i].id_volume==$(this).attr('id_volume')){
                    
                    $('.order_product_box').each(function(){
                        if($(this).attr('product_id')==basket[i].id_product && basket[i].id_taste==$(this).attr('taste_id') && basket[i].id_volume==$(this).attr('volume_id')){
                        $(this).removeClass('grey_joy');
                        }
                    });
                    
                    
                        
                    
                    }
                    else{
                        basket2.push({
                        id_product: basket[i].id_product,
                        id_taste: basket[i].id_taste,
                        id_volume: basket[i].id_volume,
                        count: basket[i].count
                        });
                    }
            }
            $.cookie('basket', JSON.stringify(basket2) , { path: '/', expires: 30 });
            $('input[number_product="'+$(this).attr('number_product')+'"]').remove();
            $(this).parent().remove();
            basket_refresh()
        //}
        
        return false;

    });

}


function change_new_count(){
    $('.this_right_count').change(function(){
        var nashli = 0;
        var basket = JSON.parse($.cookie('basket'));
        for( var i in basket){
            if(basket[i].id_product== $(this).parent().next().attr('id_product') && basket[i].id_taste==$(this).parent().next().attr('id_taste') && basket[i].id_volume==$(this).parent().next().attr('id_volume')){
                basket[i].count = $(this).val()*1;
                nashli = true;
                $('input[number_product="'+$(this).parent().next().attr('number_product')+'"][name="count[]"]').val(basket[i].count);
            }
        }
        $.cookie('basket', JSON.stringify(basket) , { path: '/', expires: 30 });
        basket_refresh()

    });
}