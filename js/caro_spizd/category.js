        jQuery(window).load(function() {
	
	//	Home page products
	jQuery('.home-products1').carouFredSel({
		align: "center",
		responsive: false,
		circular: true,
		auto: false,
		infinite: true,
	    width: '100%',
	    items: {
			visible: 3
		},
		scroll : {
			duration: 1000,
			items: 1
		},
		next : {
			button: "#next-prod1",
			key: "right"
		},
		prev : {
			button: "#prev-prod1",
			key: "left"
		}
	});
        
        jQuery('.home-products2').carouFredSel({
		align: "center",
		responsive: false,
		circular: true,
		auto: false,
		infinite: true,
	    width: '100%',
	    items: {
			visible: 3
		},
		scroll : {
			duration: 1000,
			items: 1
		},
		next : {
			button: "#next-prod2",
			key: "right"
		},
		prev : {
			button: "#prev-prod2",
			key: "left"
		}
	});
        
        jQuery('.home-products3').carouFredSel({
		align: "center",
		responsive: false,
		circular: true,
		auto: false,
		infinite: true,
	    width: '100%',
	    items: {
			visible: 3
		},
		scroll : {
			duration: 1000,
			items: 1
		},
		next : {
			button: "#next-prod3",
			key: "right"
		},
		prev : {
			button: "#prev-prod3",
			key: "left"
		}
	});

        jQuery('.home-products4').carouFredSel({
            align: "center",
            responsive: false,
            circular: true,
            auto: false,
            infinite: true,
            width: '100%',
            items: {
                visible: 3
            },
            scroll : {
                duration: 1000,
                items: 1
            },
            next : {
                button: "#next-prod4",
                key: "right"
            },
            prev : {
                button: "#prev-prod4",
                key: "left"
            }
        });


        jQuery('.home-products5').carouFredSel({
            align: "center",
            responsive: false,
            circular: true,
            auto: false,
            infinite: true,
            width: '100%',
            items: {
                visible: 3
            },
            scroll : {
                duration: 1000,
                items: 1
            },
            next : {
                button: "#next-prod5",
                key: "right"
            },
            prev : {
                button: "#prev-prod5",
                key: "left"
            }
        });



        jQuery('.home-products6').carouFredSel({
            align: "center",
            responsive: false,
            circular: true,
            auto: false,
            infinite: true,
            width: '100%',
            items: {
                visible: 3
            },
            scroll : {
                duration: 1000,
                items: 1
            },
            next : {
                button: "#next-prod6",
                key: "right"
            },
            prev : {
                button: "#prev-prod6",
                key: "left"
            }
        });



        jQuery('.home-products7').carouFredSel({
            align: "center",
            responsive: false,
            circular: true,
            auto: false,
            infinite: true,
            width: '100%',
            items: {
                visible: 3
            },
            scroll : {
                duration: 1000,
                items: 1
            },
            next : {
                button: "#next-prod7",
                key: "right"
            },
            prev : {
                button: "#prev-prod7",
                key: "left"
            }
        });



        jQuery('.home-products8').carouFredSel({
            align: "center",
            responsive: false,
            circular: true,
            auto: false,
            infinite: true,
            width: '100%',
            items: {
                visible: 3
            },
            scroll : {
                duration: 1000,
                items: 1
            },
            next : {
                button: "#next-prod8",
                key: "right"
            },
            prev : {
                button: "#prev-prod8",
                key: "left"
            }
        });

        jQuery('.home-products9').carouFredSel({
            align: "center",
            responsive: false,
            circular: true,
            auto: false,
            infinite: true,
            width: '100%',
            items: {
                visible: 3
            },
            scroll : {
                duration: 1000,
                items: 1
            },
            next : {
                button: "#next-prod9",
                key: "right"
            },
            prev : {
                button: "#prev-prod9",
                key: "left"
            }
        });

        jQuery('.home-products10').carouFredSel({
            align: "center",
            responsive: false,
            circular: true,
            auto: false,
            infinite: true,
            width: '100%',
            items: {
                visible: 3
            },
            scroll : {
                duration: 1000,
                items: 1
            },
            next : {
                button: "#next-prod10",
                key: "right"
            },
            prev : {
                button: "#prev-prod10",
                key: "left"
            }
        });

	
	
});


