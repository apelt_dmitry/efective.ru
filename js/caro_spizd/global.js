jQuery(document).ready(function() {

	jQuery('.form-list .input-box').each(function(){
	    jQuery(this).prev('label').andSelf().wrapAll('<span class="infield-label" />');
	});

	jQuery('.infield-label').infieldLabel();

	var windowWidth = jQuery(window).width();
	var windowHeight = jQuery(window).height();
	var navHeight = jQuery('#navigation-sticky-wrapper').outerHeight();
	
	if(windowWidth>767) {
		jQuery('#home-banner-wrap a').each(function() {
			jQuery(this).css('height',windowHeight);
		});
		jQuery('section#home').each(function() {
			jQuery(this).css('height',windowHeight);
		});
		
	}
	
	jQuery('.select-wrap select').each(function() {
		if(jQuery(this).is(':disabled')) {
			jQuery(this).parent('.select-wrap').addClass('disabled-select');
		}
	});
	
	jQuery(".select-wrap select" ).change(function() {
		jQuery('.select-wrap select').each(function() {
			jQuery(this).parent('.select-wrap').removeClass('disabled-select');
			if(jQuery(this).is(':disabled')) {
				jQuery(this).parent('.select-wrap').addClass('disabled-select');
			}
		});
	});

	//Fancybox Newletter Open
	/*jQuery(".email-signup").fancybox({
		maxWidth	: 300,
		maxHeight	: 400,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
*/
	
	//	Product Page
	jQuery('.row-of-products-wrap').carouFredSel({
		align: "center",
		responsive: true,
		circular: true,
		auto: false,
		infinite: true,
	    width: '100%',
	    items: {
			visible: 1
		},
		scroll : {
			duration: 800,
			fx: "crossfade",
			items: 1
		},
		swipe : {
	        onTouch : true
	    },
		next : {
			button: "#slider_single_right",
			key: "right"
		},
		prev : {
			button: "#slider_single_left",
			key: "left"
		}
	});
	
	
	//	Home Banner
	jQuery('#home-banner').carouFredSel({
		align: "center",
		responsive: true,
		circular: true,
		auto: true,
		infinite: true,
	    width: '100%',
	    height: 'auto',
	    items: {
			visible: 1
		},
		scroll : {
			duration: 1000,
			items: 1,
			fx: "crossfade",
			onAfter: function() {
				var pos = jQuery("#home-banner").triggerHandler("currentPosition");
				jQuery('#home-banner a').removeClass('selected');
				jQuery('#home-banner a#slide-'+pos).addClass('selected');
			}
		},
		next : {
			button: "#next-slide"
		},
		prev : {
			button: "#prev-slide"
		},
		swipe : {
	        onTouch : true
	    },
		pagination : {
			container		: "#slider-pagination",
			anchorBuilder	: function(nr, item) {
				return "<div><span></span></div>";
			}
		}
	});
	
	jQuery(window).resize(function() {
		var windowHeight = jQuery(window).height();
		var windowWidth = jQuery(window).width();
		if(windowWidth>767){
			jQuery('#home-banner-wrap .caroufredsel_wrapper, #home-banner, #home-banner a, section#home').css({
				height: windowHeight
			}).removeClass('small-banner');
		}else{
			jQuery('#home-banner-wrap .caroufredsel_wrapper, #home-banner, #home-banner a, section#home').css({
				height: "500px"
			}).addClass('small-banner');
		}
	});
	
	jQuery('#home-banner a#slide-0').addClass('selected');
	
	//Adding stling to radio and checkboxes
    jQuery('body').addClass('has-js');
    jQuery('.label_check, .label_radio').click(function(){
        setupLabel();
    });
    setupLabel();
	
});

//Adding stling to radio and checkboxes
function setupLabel() {
    if (jQuery('.label_check input').length) {
        jQuery('.label_check').each(function(){ 
            jQuery(this).removeClass('c_on');
        });
        jQuery('.label_check input:checked').each(function(){ 
            jQuery(this).parent('label').addClass('c_on');
        });                
    };
    if (jQuery('.label_radio input').length) {
        jQuery('.label_radio').each(function(){ 
            jQuery(this).removeClass('r_on');
        });
        jQuery('.label_radio input:checked').each(function(){ 
            jQuery(this).parent('label').addClass('r_on');
        });
    };
};
